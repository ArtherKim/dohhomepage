import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/app/router.gr.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

@lazySingleton
class NavigationFunctionModel extends BaseViewModel {
  final NavigationService navigationService = locator<NavigationService>();

  Future navigateToHome(StartupViewModel startupViewModel) async {
    await navigationService.navigateTo(Routes.homeView,
        arguments: HomeViewArguments(startupViewModel: startupViewModel));
  }

  Future navigateToChurchInfo(StartupViewModel startupViewModel) async {
    await navigationService.navigateTo(Routes.churchInfoView,
        arguments: ChurchInfoViewArguments(startupViewModel: startupViewModel));
  }

  Future navigateToStartup() async {
    await navigationService.navigateTo(Routes.startupView);
  }

  Future navigateToPushTest(String id) async {
    await navigationService.navigateTo(Routes.pushNotificationTest,
        arguments: PushNotificationTestArguments(postId: id));
  }
}