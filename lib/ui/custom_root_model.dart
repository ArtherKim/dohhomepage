import 'dart:async';

import 'package:ars_progress_dialog/dialog.dart';
import 'package:doh_homepage_app/api/rest_api_helper.dart';
import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/service/shared_preference.dart';
import 'package:doh_homepage_app/ui/luncher_function_model.dart';
import 'package:doh_homepage_app/ui/navigation_function_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class CustomRootModel extends IndexTrackingViewModel  {
  final NavigationFunctionModel navigationModel = locator<NavigationFunctionModel>();
  final URLLauncherModel urlLLauncherModel = locator<URLLauncherModel>();
  final RestAPIHelper restAPIHelper = locator<RestAPIHelper>();
  final StorageUtil storageUtil = locator<StorageUtil>();
  final DialogService dialogService = locator<DialogService>();
  final SnackbarService snackBarService = locator<SnackbarService>();
  final StartupViewModel startupViewModel = locator<StartupViewModel>();

  ArsProgressDialog pr;

  bool _isAnimated = true;
  get isAnimated => _isAnimated;

  int _time = 0;
  Timer _timer;

  ItemScrollController _itemScrollController = ItemScrollController();
  get itemScrollController => _itemScrollController;

  void swipePage(int pageNo, int tabNo, StartupViewModel startupViewModel){
    startupViewModel.onTab(pageNo, tabNo,);
    notifyListeners();
  }

  getStorageInstance() async{
    await storageUtil.getInstance();
    notifyListeners();
  }

  prReady(BuildContext context) async{
    pr = ArsProgressDialog(
        context,
        blur: 2,
        backgroundColor: Color(0x33000000),
        animationDuration: Duration(milliseconds: 500),
    );
    notifyListeners();
  }

  String dateToString(DateTime date){
    return DateFormat('yyyy-MM-dd').format(date);
  }

  _startTimer(){
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      _time++;
      if(_time > 4) {
        _resetTimer();
        _isAnimated = true;
        notifyListeners();
      }
    });
  }

  changeIsAnimated(bool value) async{
    _isAnimated = value;
    if(_isAnimated == false) {
      _startTimer();
    } else {
      _resetTimer();
    }
    notifyListeners();
  }

  _resetTimer(){
    _timer.cancel();
    _time = 0;
    notifyListeners();
  }
}