import 'package:flutter/material.dart';

class PushNotificationTest extends StatefulWidget {
  final String postId;

  const PushNotificationTest({Key key, this.postId}) : super(key: key);
  @override
  _PushNotificationTestState createState() => _PushNotificationTestState();
}

class _PushNotificationTestState extends State<PushNotificationTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('notification Test'),
      ),
      body: Center(
        child: Text(widget.postId),
      ),
    );
  }
}
