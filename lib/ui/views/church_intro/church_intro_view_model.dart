import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:doh_homepage_app/model/posts/base_req.dart';
import 'package:doh_homepage_app/model/posts/church_intro/church_intro_res.dart';
import 'package:doh_homepage_app/model/posts/history/history_res.dart';
import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChurchIntroViewModel extends CustomRootModel {
  ChurchIntroRes _churchIntroRes = ChurchIntroRes(
    postId: 0,
    about: '준비중입니다.',
  );
  get churchIntroRes => _churchIntroRes;

  List<StaffRes> _staffRes;
  get staffRes => _staffRes;

  int _listStaffLength = 0;
  get listStaffLength => _listStaffLength;

  DateTime _startDate = DateTime(2012, 01, 01);
  get startDate => _startDate;

  DateTime _endDate = DateTime.now();
  get endDate => _endDate;

  DateTime _temStartDate = DateTime(2012,1,1);
  DateTime _temEndDate = DateTime.now();

  List<HistoryRes> _historyRes = [];
  get historyRes => _historyRes;

  bool _isGetData = false;
  get isGetData => _isGetData;


  onModelReady(ScrollController scrCtr) async {
    ///네트워크 테스트용
    //_getIntroChurchFromAjax(restAPIHelper.wpAjax);
    if (storageUtil.churchIntro == null || storageUtil.historyRes == null) {
      /// 내부 스토리지의 저장 데이터가 없을경우 ajax 요청
      _getIntroChurchFromAjax(restAPIHelper.wpAjax);
    } else {
      /// 내부 스토리지 저장 데이터 불러오기
      _churchIntroRes.about = storageUtil.churchIntro.about;
      _historyRes = storageUtil.historyRes;
      _getIntroChurchFromStorage(storageUtil.churchIntro);
      _getHistoryFromAjax();
      _isGetData = true;
    }
    notifyListeners();
  }

  void _getIntroData(ChurchIntroRes churchIntroRes) {
    _staffRes = [
      for (int i = 0; i < churchIntroRes.staff.length; i++)
        churchIntroRes.staff["$i"],
    ];
    _listStaffLength = _staffRes.length;
    notifyListeners();
  }

  _getIntroChurchFromStorage(ChurchIntroRes churchIntroRes) {
    _getIntroData(churchIntroRes);
    notifyListeners();
  }

  _getIntroChurchFromAjax(String url) async {
    pr.show();
    BaseReq baseReq = BaseReq(
      action: "load_posts_by_ajax",
      postType: "page",
      pageId: "453",
      categoryName: "intro-of-church",
      security: storageUtil.securityCode,
    );
    restAPIHelper.fetchPost(baseReq, url: url).then((value) async {
      Map valueMap = json.decode(value.data);
      _churchIntroRes = ChurchIntroRes.fromJson(valueMap["0"]);
      _getIntroData(_churchIntroRes);
      await _getHistoryFromAjax();
      storageUtil.setChurchIntroData(_churchIntroRes);

      ///내부 스토리지 데이터 저장
    }).catchError((e) {
      print(e);
    }).whenComplete(() {
      if (pr.isShowing) pr.dismiss();
    });
    notifyListeners();
  }

  showPlatformDatePicker(BuildContext context, String type) async {
    if(Platform.isIOS) {
      _showCupertinoDateTimePicker(context, type);
    } else {
      _showDateTimePicker(context, type);
    }
  }

  _showDateTimePicker(BuildContext context, String type) async {
    var picked  = await showDatePicker(
      context: context,
      initialDate: type.toLowerCase() == 'start' ? _startDate : _endDate,
      firstDate: DateTime(2012,1,1),
      lastDate: DateTime(DateTime.now().year,12,31),
    );
    if (picked != null) if (type.toLowerCase() == 'start') {
      if (picked.isBefore(_endDate)) {
        _startDate = picked;
        await _getHistoryFromAjax();
      } else {
        await showsnackbar(
            message: '날짜를 다시 확인 후 선택하여 주십쇼'
        );
      }
    } else {
      if (picked.isAfter(_endDate)) {
        _endDate = picked;
        await _getHistoryFromAjax();
      } else {
        await showsnackbar(
            message: '날짜를 다시 확인 후 선택하여 주십쇼'
        );
      }
    }
    notifyListeners();
  }

  _showCupertinoDateTimePicker(BuildContext context, String type) async {
    showCupertinoModalPopup (
        context: context,
        builder: (_) => Container(
          height: 250,
          color: Colors.white,
          child: Stack(
            children: [
              CupertinoDatePicker(
                mode: CupertinoDatePickerMode.date,
                initialDateTime: type.toLowerCase() == 'start' ? _startDate : _endDate,
                maximumDate: DateTime(DateTime.now().year,12,31),
                minimumDate: DateTime(2012,1,1),
                onDateTimeChanged: (DateTime value) {
                  if(type.toLowerCase() == 'start') {
                    _temStartDate = value;
                  } else {
                    _temEndDate = value;
                  }
                  notifyListeners();
                },
              ),
              Positioned(
                right: 0,
                top: 0,
                child: CupertinoButton(
                  child: Text('Select'),
                  onPressed: () async {
                    if(type.toLowerCase() == 'start') {
                      if(_temStartDate.isBefore(_endDate)){
                        _startDate = _temStartDate;
                        navigationModel.navigationService.back();
                        await _getHistoryFromAjax();
                      } else {
                        await showsnackbar(
                            message: '날짜를 다시 확인 후 선택하여 주십쇼'
                        );
                      }
                    } else {
                      if(_temEndDate.isAfter(_startDate)){
                        _endDate = _temEndDate;
                        navigationModel.navigationService.back();
                        await _getHistoryFromAjax();
                      } else {
                        await showsnackbar(
                            message: '날짜를 다시 확인 후 선택하여 주십쇼'
                        );
                      }
                    }
                    notifyListeners();
                  },
                ),
              )
            ],
          ),
        )
    );
  }

  _getHistoryFromAjax() async {
    await changeIsEmptyData(false);
    await changeIsGetData(false);
    BaseReq baseReq = BaseReq(
      paged: 1,
      postPerPage: -1,
      categoryName: "history-of-church",
      metaKey: "date_start",
      hasRangeCondition: true,
      metaQueryKey: "date_start",
      startDate: dateToString(_startDate),
      endDate: dateToString(_endDate),
      security: storageUtil.securityCode,
    );
    restAPIHelper.fetchPost(baseReq, url: restAPIHelper.wpAjax).then((value) {
      Map valueMap = json.decode(value.data);
      List reverseList = [for (int i = 0; i < valueMap.length; i++)
        HistoryRes.fromJson(valueMap["$i"]),].reversed.toList();
      _historyRes = reverseList;
      storageUtil.setHistoryData(_historyRes);
      notifyListeners();
    }).catchError((e) {
      changeIsEmptyData(true);
    }).whenComplete(() {
      changeIsGetData(true);
    });
    notifyListeners();
  }

  bool _isEmptyData = false;
  get isEmptyData => _isEmptyData;

  changeIsEmptyData(bool value) {
    _isEmptyData = value;
    notifyListeners();
  }

  changeIsGetData(bool value) {
    _isGetData = value;
    notifyListeners();
  }

  Future showsnackbar({String message}) async{
    snackBarService.showSnackbar(
      title: '선택날짜가 유호하지 않습니다',
      message: message,
      duration: Duration(seconds: 3),
    );
  }
}
