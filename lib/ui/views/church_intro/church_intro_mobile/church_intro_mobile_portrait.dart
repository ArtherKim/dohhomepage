import 'package:doh_homepage_app/ui/views/church_intro/church_intro_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/custom_view_box/custom_view_box_layout.dart';
import 'package:doh_homepage_app/widgets/locator_button/locator_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:stacked/stacked.dart';
import 'package:animated_widgets/animated_widgets.dart';
import 'package:swipedetector/swipedetector.dart';

class ChurchIntroMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;

  const ChurchIntroMobilePortrait({Key key, this.startupViewModel})
      : super(key: key);

  @override
  _ChurchIntroMobilePortraitState createState() =>
      _ChurchIntroMobilePortraitState();
}

class _ChurchIntroMobilePortraitState extends State<ChurchIntroMobilePortrait>
    with TickerProviderStateMixin {
  List<String> titles = ['하늘그림교회는?','교회연혁','직분'];
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChurchIntroViewModel>.reactive(
      viewModelBuilder: () => ChurchIntroViewModel(),
      onModelReady: (model) => onModelInit(model),
      builder: (context, model, child) => Stack(
        children: [
          GestureDetector(
            onTap: (){
              if(model.isAnimated == false){
                model.changeIsAnimated(true);
              }
            },
            child: ScrollablePositionedList.builder(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 10),
              physics: ClampingScrollPhysics(),
              itemScrollController: model.itemScrollController,
              itemCount: bodyWidgets(model).length,
              itemBuilder: (context, index) {
                return bodyWidgets(model)[index];
              },
            ),
          ),
          Positioned(
            bottom: 20,
            right: 0,
            child: GestureDetector(
                onTap: () {
                  if (model.isAnimated) {
                    model.changeIsAnimated(false);
                  }
                },
                child: LocatorButtonFrontWidget(
                  enabled: model.isAnimated,
                )
            ),
          ),
          Positioned(
            bottom: 20,
            right: 0,
            child: GestureDetector(
                onTap: () {
                  if (model.isAnimated == false) {
                    model.changeIsAnimated(true);
                  }
                },
                child: LocatorButtonBackWidget(
                  enabled: model.isAnimated,
                  widgetList: titles,
                  itemScrollController: model.itemScrollController,
                ),
            )
          ),
        ],
      ),
    );
  }

  List<Widget> bodyWidgets(ChurchIntroViewModel model) {
    return [
      CustomViewBoxLayout(
        titleArea: CustomViewBoxTitleLayout(
          title: titles[0],
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            model.churchIntroRes.about,
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        titleArea: CustomViewBoxTitleLayout(
          title: titles[1],
        ),
        buttonsArea: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Row(
            children: [
              InkWell(
                onTap: () async {
                  await model.showPlatformDatePicker(context, 'start');
                },
                child: Container(
                  height: 40,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                        color: const Color(0xFF599ba2),
                      )),
                  child:
                      Center(child: Text(model.dateToString(model.startDate))),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 10, right: 10),
                width: 50,
                height: 40,
                child: FittedBox(child: Center(child: Text('부터'))),
              ),
              InkWell(
                onTap: () async {
                  await model.showPlatformDatePicker(context, 'end');
                },
                child: Container(
                  height: 40,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                        color: const Color(0xFF599ba2),
                      )),
                  child: Center(child: Text(model.dateToString(model.endDate))),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 10, right: 10),
                width: 50,
                height: 40,
                child: FittedBox(child: Center(child: Text('까지'))),
              ),
            ],
          ),
        ),
        bodyArea: CustomViewBoxBodyLayout(
          child: model.isEmptyData
              ? Text('해당 날짜의 영혁이 존재하지 않습니다')
              : model.isGetData
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        for (int i = 0; i < model.historyRes.length; i++)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 15.0),
                                child: SizedBox(
                                  width: (UiStyle.width - 70) / 3,
                                  child: Text(
                                    model.historyRes[i].postTitle,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  for (int j = 0;
                                      j < model.historyRes[i].content.length;
                                      j++)
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 15.0),
                                      child: SizedBox(
                                        width: (UiStyle.width - 70) / 1.8,
                                        child: Text(
                                          model.historyRes[i].content["$j"],
                                          style: TextStyle(fontSize: 12),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
        ),
      ),
      CustomViewBoxLayout(
        titleArea: CustomViewBoxTitleLayout(
          title: titles[2],
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: model.listStaffLength != 0
              ? Column(
                  children: [
                    for (int i = 0; i < model.listStaffLength; i++)
                      introCard(model, i),
                  ],
                )
              : Text(
                  '준비중입니다.',
                  style: TextStyle(fontSize: 12),
                ),
        ),
      ),
    ];
  }

  onModelInit(ChurchIntroViewModel model) async {
    await model.prReady(context);
    model.onModelReady(widget.startupViewModel.scrollerController);
    //if(model.isAnimated == false) model.changeIsAnimated(true);
  }

  Widget introCard(ChurchIntroViewModel model, int i) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: UiStyle.safeBlockHorizontal * 20,
          decoration: BoxDecoration(
              color: const Color(0xff489FAA),
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                topLeft: Radius.circular(10),
              )),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: Center(
                child: Text(
              model.staffRes[i].title,
              style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
            )),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: UiStyle.activeTabColor,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                )),
            child: Padding(
                padding: const EdgeInsets.fromLTRB(25, 10, 10, 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 5.0),
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.userAlt,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      model.staffRes[i].name,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Icon(
                                  FontAwesomeIcons.solidEnvelopeOpen,
                                  color: Colors.white,
                                  size: 18,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    'drawingheaven@hanmail.net',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 13),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Icon(
                          FontAwesomeIcons.solidCopy,
                          size: 40,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                )),
          ),
        )
      ],
    );
  }
}
