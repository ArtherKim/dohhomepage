import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:doh_homepage_app/ui/views/church_intro/church_intro_view_model.dart';
import 'package:doh_homepage_app/ui/views/church_life/church_life_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/custom_view_box/custom_view_box_layout.dart';
import 'package:doh_homepage_app/widgets/locator_button/locator_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:stacked/stacked.dart';

class ChurchLifeMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;

  const ChurchLifeMobilePortrait({Key key, this.startupViewModel})
      : super(key: key);

  @override
  _ChurchLifeMobilePortraitState createState() => _ChurchLifeMobilePortraitState();
}

class _ChurchLifeMobilePortraitState extends State<ChurchLifeMobilePortrait> {
  List<String> titles = [
    '예배&모임',
    '새가족',
    '목장',
    '심방',
    '성찬',
    '세례',
    '절기',
    '헌금',
    '결혼&장례',
    '섬김'
  ];
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChurchLifeViewModel>.reactive(
      onModelReady: (model) => onModelInit(model),
      viewModelBuilder: () => ChurchLifeViewModel(),
      builder: (context, model, child) => Stack(
        children: [
          GestureDetector(
            onTap: () {
              if (model.isAnimated == false) {
                model.changeIsAnimated(true);
              }
            },
            child: ScrollablePositionedList.builder(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 10),
              physics: ClampingScrollPhysics(),
              itemScrollController: model.itemScrollController,
              itemCount: bodyWidgets(model).length,
              itemBuilder: (context, index) {
                return bodyWidgets(model)[index];
              },
            ),
          ),
          Positioned(
            bottom: 20,
            right: 0,
            child: GestureDetector(
                onTap: () {
                  if (model.isAnimated) {
                    model.changeIsAnimated(false);
                  }
                },
                child: LocatorButtonFrontWidget(
                  enabled: model.isAnimated,
                )),
          ),
          Positioned(
            bottom: 20,
            right: 0,
            child: GestureDetector(
              onTap: () {
                if (model.isAnimated == false) {
                  model.changeIsAnimated(true);
                }
              },
              child: LocatorButtonBackWidget(
                enabled: model.isAnimated,
                widgetList: titles,
                itemScrollController: model.itemScrollController,
              ),
            ),
          ),
        ],
      ),
    );
  }

  onModelInit(ChurchLifeViewModel model) async {
    await model.prReady(context);
    /*model.onModelReady(widget.startupViewModel.scrollerController);*/
    //if(model.isAnimated == false) model.changeIsAnimated(true);
  }

  List<Widget> bodyWidgets(ChurchLifeViewModel model) {
    return [
      CustomViewBoxLayout(
        //globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '예배&모임',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '새가족',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '목장',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '심방',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '성찬',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '세례',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '절기',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '헌금',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '결혼&장례',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
      CustomViewBoxLayout(
        // globalKey: model.firstKey,
        titleArea: CustomViewBoxTitleLayout(
          title: '섬김',
        ),
        buttonsArea: Container(),
        bodyArea: CustomViewBoxBodyLayout(
          child: Text(
            '준비중입니다.',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ),
    ];
  }

/*  Widget animatedButton2(ChurchLifeViewModel model) {
    return TranslationAnimatedWidget(
      enabled: model.isAnimated,
      values: [
        Offset(0, 0),

        /// disabled value value
        Offset(200, 0),

        /// intermediate value
        Offset(200, 0)
      ],
      child: floatingButton2(model),
    );
  }

  Widget floatingButton2(ChurchLifeViewModel model) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      width: UiStyle.safeBlockHorizontal * 35,
      decoration: BoxDecoration(
          color: const Color(0xff555555),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            topLeft: Radius.circular(20),
          )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 0,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '예배&모임?',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 1,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '새가족',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 2,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '목장',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 3,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '심방',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 4,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '성찬',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 5,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '세례',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 6,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '절기',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 7,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '헌금',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 8,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '결혼&장례',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              model.itemScrollController.scrollTo(
                  index: 9,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.decelerate);
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                '섬김',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }*/
}
