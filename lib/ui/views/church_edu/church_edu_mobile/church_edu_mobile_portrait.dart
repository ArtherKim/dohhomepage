import 'package:doh_homepage_app/ui/views/church_edu/church_edu_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ChurchEduMobilePortrait extends StatelessWidget {
  final StartupViewModel startupViewModel;

  const ChurchEduMobilePortrait({Key key, this.startupViewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChurchEduViewModel>.reactive(
      viewModelBuilder: () => ChurchEduViewModel(),
      builder: (context, model, child) => Scaffold(
        body: Text('aa'),
      ),
    );
  }
}
