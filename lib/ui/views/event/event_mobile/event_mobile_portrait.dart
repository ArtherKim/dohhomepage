import 'package:doh_homepage_app/ui/views/event/event_view_model.dart';
import 'package:doh_homepage_app/ui/views/sermon/sermon_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/ui/views/test.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/app_bar/app_bar_widget.dart';
import 'package:doh_homepage_app/widgets/custom_sermon_box/custom_sermon_box_widget.dart';
import 'package:doh_homepage_app/widgets/filter_calendar/filter_calendar_widget.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_timetable_view/flutter_timetable_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:table_calendar/table_calendar.dart';

class EventMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;
  const EventMobilePortrait({Key key, this.startupViewModel}) : super(key: key);

  @override
  _EventMobilePortraitState createState() => _EventMobilePortraitState();
}

class _EventMobilePortraitState extends State<EventMobilePortrait> with TickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<EventViewModel>.reactive(
      onModelReady: (model) => model.onModelInit(this),
        viewModelBuilder: () => EventViewModel(),
        builder: (context, model, child) => Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(UiStyle.blockSizeHorizontal * 20),
            child: AppBarLayout(
              title: '교회일정',
              icon: FontAwesomeIcons.calendar,
              backgroundColor: Color(0xffe4c088),
              button1: TailButtonLayout(
                width: UiStyle.safeBlockHorizontal*12,
                title: '월',
                backgroundColor: Color(0xffb5996d),
                onTap: ()=> model.calendarSetting(CalendarFormat.month),
              ),
              button2: TailButtonLayout(
                width: UiStyle.safeBlockHorizontal*12,
                title: '2주',
                backgroundColor: Color(0xffb5996d),
                onTap: ()=> model.calendarSetting(CalendarFormat.twoWeeks),
              ),
              button3: TailButtonLayout(
                width: UiStyle.safeBlockHorizontal*12,
                title: '주',
                backgroundColor: Color(0xffb5996d),
                onTap: ()=> model.calendarSetting(CalendarFormat.week),
              ),
            ),
          ),
          body:Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: FilterCalendarWidget(
                  calendarController: model.calCtr,
                  animationController: model.aniCtr,
                  events: model.events,
                  onCalendarCreated: model.onCalendarCreated,
                  onDaySelected: model.onDaySelected,
                  onVisibleDaysChanged: model.onVisibleDaysChanged,
                ),
              ),
              const SizedBox(height: 8.0),
              Expanded(child: _buildEventList(model)),
            ],
          ),
        )
    );
  }

  Widget _buildEventList(EventViewModel model) {
    return ListView(
      physics: ClampingScrollPhysics(),
      children: model.selectedEvents.map<Widget>((event) => Container(
        decoration: BoxDecoration(
          border: Border.all(width: 0.8),
          borderRadius: BorderRadius.circular(12.0),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: ListTile(
          title: Text(event.toString()),
          onTap: () => print('$event tapped!'),
        ),
      )).toList(),
    );
  }
}
