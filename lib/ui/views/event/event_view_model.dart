import 'package:doh_homepage_app/ui/luncher_function_model.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class EventViewModel extends URLLauncherModel{

  CalendarController _calCtr;
  get calCtr => _calCtr;

  AnimationController _aniCtr;
  get aniCtr => _aniCtr;

  DateTime _selectedDay = DateTime.now();
  get selectedDay => _selectedDay;

  Map<DateTime,List> _events;
  get events => _events;

  List<dynamic> _selectedEvents;
  get selectedEvents => _selectedEvents;

  onModelInit(TickerProvider tickerProvider){
    _calCtr = CalendarController();
    _aniCtr = AnimationController(
      duration: const Duration(microseconds: 400),
      vsync: tickerProvider,
    );
    getEventLists();
    _aniCtr.forward();
    notifyListeners();
  }

  void getEventLists(){
    _events = {
      _selectedDay.subtract(Duration(days: 30)): [
        'Event A0',
        'Event B0',
        'Event C0'
      ],
      _selectedDay.subtract(Duration(days: 27)): ['Event A1'],
      _selectedDay.subtract(Duration(days: 20)): [
        'Event A2',
        'Event B2',
        'Event C2',
        'Event D2'
      ],
      _selectedDay.subtract(Duration(days: 16)): ['Event A3', 'Event B3'],
      _selectedDay.subtract(Duration(days: 10)): [
        'Event A4',
        'Event B4',
        'Event C4'
      ],
      _selectedDay.subtract(Duration(days: 4)): [
        'Event A5',
        'Event B5',
        'Event C5'
      ],
      _selectedDay.subtract(Duration(days: 2)): ['Event A6', 'Event B6'],
      _selectedDay: ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
      _selectedDay.add(Duration(days: 1)): [
        'Event A8',
        'Event B8',
        'Event C8',
        'Event D8'
      ],
      _selectedDay.add(Duration(days: 3)):
      Set.from(['Event A9', 'Event A9', 'Event B9']).toList(),
      _selectedDay.add(Duration(days: 7)): [
        'Event A10',
        'Event B10',
        'Event C10'
      ],
      _selectedDay.add(Duration(days: 11)): ['Event A11', 'Event B11'],
      _selectedDay.add(Duration(days: 17)): [
        'Event A12',
        'Event B12',
        'Event C12',
        'Event D12'
      ],
      _selectedDay.add(Duration(days: 22)): ['Event A13', 'Event B13'],
      _selectedDay.add(Duration(days: 26)): [
        'Event A14',
        'Event B14',
        'Event C14'
      ],
    };
    _selectedEvents = _events[_selectedDay] ?? [];
    notifyListeners();
  }

  void onDaySelected(DateTime day, List events, List holidays) {
    _selectedEvents = events;
    notifyListeners();
  }

  void calendarSetting(CalendarFormat format){
    _calCtr.setCalendarFormat(format);
    notifyListeners();
  }

  void onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    //print('CALLBACK: _onVisibleDaysChanged');
  }

  void onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {
    //print('CALLBACK: _onCalendarCreated');
  }
}