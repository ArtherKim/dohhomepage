import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/ui/views/church_intro/church_intro_mobile/church_intro_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/church_life/church_life_mobile/church_life_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/event/event_mobile/event_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';

class EventView extends StatelessWidget {
  final StartupViewModel startupViewModel;
  const EventView({Key key, this.startupViewModel,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: EventMobilePortrait(startupViewModel: startupViewModel,),
        //landscape: ChurchLifeMobilePortrait(),
      ),
      /*tablet: OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),
      desktop:OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),*/
    );
  }
}
