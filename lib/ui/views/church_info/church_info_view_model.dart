import 'dart:convert';

import 'package:doh_homepage_app/api/rest_api_helper.dart';
import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/model/posts/base_req.dart';
import 'package:doh_homepage_app/model/posts/base_res.dart';
import 'package:doh_homepage_app/model/posts/church_intro/church_intro_res.dart';
import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/scheduler/ticker.dart';

class ChurchInfoViewModel extends CustomRootModel{
  TabController _tabController;
  get tabController => _tabController;

  void onModelReady(int tabNo){
    _pageNo = tabNo;
    notifyListeners();
  }

  Color firstTabColor(int index){
    if(index == 0){
      return UiStyle.activeTabColor;
    }
    return Colors.grey;
  }

  Color secTabColor(int index){
    if(index == 1){
      return UiStyle.activeTabColor;
    }
    return Colors.grey;
  }
  Color thirdTabColor(int index){
    if(index == 2){
      return UiStyle.activeTabColor;
    }
    return Colors.grey;
  }

  int _pageNo = 0;
  get pageNo => _pageNo;


  void onTab(int index){
    _pageNo = index;
    notifyListeners();
  }
}