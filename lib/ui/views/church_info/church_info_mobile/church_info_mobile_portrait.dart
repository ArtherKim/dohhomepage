import 'package:doh_homepage_app/ui/views/church_edu/church_edu_view.dart';
import 'package:doh_homepage_app/ui/views/church_info/church_info_view_model.dart';
import 'package:doh_homepage_app/ui/views/church_intro/church_intro_view.dart';
import 'package:doh_homepage_app/ui/views/church_life/church_life_view.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/responsive_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:swipedetector/swipedetector.dart';

class ChurchInfoMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;

  const ChurchInfoMobilePortrait({
    Key key,
    this.startupViewModel,
  }) : super(key: key);

  @override
  _ChurchInfoMobilePortraitState createState() =>
      _ChurchInfoMobilePortraitState();
}

class _ChurchInfoMobilePortraitState extends State<ChurchInfoMobilePortrait>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChurchInfoViewModel>.reactive(
      viewModelBuilder: () => ChurchInfoViewModel(),
      onModelReady: (model) => model.onModelReady(
        widget.startupViewModel.tabNo,
      ),
      builder: (context, model, child) => Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(UiStyle.blockSizeHorizontal * 25),
          child: AppBar(
            backgroundColor: Colors.white,
            title: Center(
              child: SizedBox(
                  height: UiStyle.blockSizeHorizontal * 10,
                  child: Image.asset(
                    'img/home_view_img/logo.png',
                    fit: BoxFit.fill,
                  )),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(0.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    tapBarWidget(
                      onTap: () => model.onTab(0),
                      iconData: FontAwesomeIcons.church,
                      color: model.firstTabColor(model.pageNo),
                      title: '교회소개',
                    ),
                    tapBarWidget(
                      onTap: () => model.onTab(1),
                      iconData: FontAwesomeIcons.fish,
                      color: model.secTabColor(model.pageNo),
                      title: '교회생활',
                    ),
                    tapBarWidget(
                      onTap: () => model.onTab(2),
                      iconData: FontAwesomeIcons.graduationCap,
                      color: model.thirdTabColor(model.pageNo),
                      title: '교육훈련',
                    ),
                  ]),
            ),
            elevation: 0.0,
            centerTitle: true,
            automaticallyImplyLeading: false,
          ),
        ),
        body: SwipeDetector(
            onSwipeLeft: () {
              model.swipePage(2, 0, widget.startupViewModel);
            },
            onSwipeRight: () {
              model.swipePage(0, 0, widget.startupViewModel);
            },
            child: pageList(model)),
      ),
    );
  }

  Widget pageList(ChurchInfoViewModel model) {
    switch (model.pageNo) {
      case 0:
        return ChurchIntroView(
          startupViewModel: widget.startupViewModel,
        );
      case 1:
        return ChurchLifeView(
          startupViewModel: widget.startupViewModel,
        );
      case 2:
        return ChurchEduView(
          startupViewModel: widget.startupViewModel,
        );
      default:
        return ChurchIntroView(
          startupViewModel: widget.startupViewModel,
        );
    }
  }

  Widget tapBarWidget(
      {Function() onTap, String title, IconData iconData, Color color}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: UiStyle.blockSizeHorizontal * 33,
        height: UiStyle.blockSizeHorizontal * 12,
        color: color,
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              iconData,
              color: Colors.white,
              size: UiStyle.blockSizeHorizontal * 6,
            ),
            ResponsiveTextWidget(
              height: UiStyle.blockSizeHorizontal * 6.5,
              width: UiStyle.blockSizeHorizontal * 17,
              title: title,
              fontColor: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
