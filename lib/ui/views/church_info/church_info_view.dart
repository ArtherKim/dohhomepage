import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/ui/views/church_info/church_info_mobile/church_info_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_portrait.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';

class ChurchInfoView extends StatelessWidget {
  const ChurchInfoView({Key key, this.startupViewModel,}) : super(key: key);
  final StartupViewModel startupViewModel;

  @override
  Widget build(BuildContext context) {
    Size sizeInformation = MediaQuery.of(context).size;
    UiStyle().init(sizeInformation, context);

    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: ChurchInfoMobilePortrait(startupViewModel: startupViewModel,),
        //landscape: ChurchInfoMobilePortrait(),
      ),
      /*tablet: OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),
      desktop:OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),*/
    );
  }
}
