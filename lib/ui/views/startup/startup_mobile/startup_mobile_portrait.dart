import 'package:animations/animations.dart';
import 'package:doh_homepage_app/ui/views/church_info/church_info_view.dart';
import 'package:doh_homepage_app/ui/views/event/event_view.dart';
import 'package:doh_homepage_app/ui/views/home/home_view.dart';
import 'package:doh_homepage_app/ui/views/sermon/sermon_view.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/ui/views/test.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/bottom_navigation_bar/bottom_tap_bar_mobile_layout.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';

class StartupMobilePortrait extends StatefulWidget {
  const StartupMobilePortrait({
    Key key,
  }) : super(key: key);

  @override
  _StartupMobilePortraitState createState() => _StartupMobilePortraitState();
}

class _StartupMobilePortraitState extends State<StartupMobilePortrait>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartupViewModel>.reactive(
      viewModelBuilder: () => StartupViewModel(),
      onModelReady: (model) => model.onModelReady(),
      builder: (context, model, child) => Scaffold(
        appBar: PreferredSize( /// status bar 를 위한 appBar
          preferredSize: Size.fromHeight(0.0), /// status bar area 지정
          child: AppBar(
            backgroundColor: Colors.white, /// status bar 배경색
            brightness: Brightness.light, /// status bar 택스트 색상(light = black, dark = white)
            elevation: 0.0,
          ),
        ),
        body: SafeArea(
          child: PageTransitionSwitcher(
              duration: const Duration(milliseconds: 500),
              reverse: model.reversePage,
              transitionBuilder: (
                Widget child,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
              ) {
                return SharedAxisTransition(
                    child: child,
                    animation: animation,
                    secondaryAnimation: secondaryAnimation,
                    transitionType: SharedAxisTransitionType.horizontal);
              },
              child: pageList(model)),
        ),
        bottomNavigationBar: Container(
          color: Colors.black, /// 최하단 bar
          child: SafeArea(
            child: Stack(
              children: [
                Container(
                  height: UiStyle.safeBlockVertical * 7,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BottomTapBarMobileLayout(
                        onTap: () {
                          model.onTab(0, 0);
                          model.toggleCard(0);
                        },
                        onDoubleTap: () => model.scrollAnimated(),
                        child: BottomNavigationItemMobile(
                          icons: Icon(
                            FontAwesomeIcons.home,
                            size: 25,
                            color: model.firstTabColor(model.pageNo),
                          ),
                          color: UiStyle.backgroundBottomNavigationHomeColor,
                        ),
                      ),
                      FlipCard(
                        key: model.cardKey1,
                        direction: FlipDirection.VERTICAL,
                        front: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(1, 0),
                          child: BottomNavigationItemMobile(
                            icons: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(3.0),
                                  child: Icon(FontAwesomeIcons.church,
                                      size: 13,
                                      color:
                                          model.secondTabColor(model.pageNo)),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(FontAwesomeIcons.fish,
                                          size: 13,
                                          color: model
                                              .secondTabColor(model.pageNo)),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(
                                          FontAwesomeIcons.graduationCap,
                                          size: 13,
                                          color: model
                                              .secondTabColor(model.pageNo)),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        back: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(1, 0),
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.child,
                              size: 25,
                              color: model.secondTabColor(model.pageNo),
                            ),
                            color: UiStyle.childSelectionColor,
                          ),
                        ),
                      ),
                      FlipCard(
                        key: model.cardKey2,
                        direction: FlipDirection.VERTICAL,
                        front: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(2, 0),
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.bible,
                              size: 25,
                              color: model.thirdTabColor(model.pageNo),
                            ),
                          ),
                        ),
                        back: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(2, 0),
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.solidStickyNote,
                              size: 25,
                              color: model.thirdTabColor(model.pageNo),
                            ),
                          ),
                        ),
                      ),
                      FlipCard(
                        key: model.cardKey3,
                        direction: FlipDirection.VERTICAL,
                        front: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(3, 0),
                          // color: UiStyle.backgroundBottomNavigationOthersColor,
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.calendarAlt,
                              size: 25,
                              color: model.fourthTabColor(model.pageNo),
                            ),
                          ),
                        ),
                        back: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(3, 0),
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.scroll,
                              size: 25,
                              color: model.fourthTabColor(model.pageNo),
                            ),
                          ),
                        ),
                      ),
                      FlipCard(
                        key: model.cardKey4,
                        direction: FlipDirection.VERTICAL,
                        front: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(4, 0),
                          // color: UiStyle.backgroundBottomNavigationOthersColor,
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.solidImages,
                              size: 25,
                              color: model.fifthTabColor(model.pageNo),
                            ),
                          ),
                        ),
                        back: BottomTapBarMobileLayout(
                          onTap: () => model.onTab(4, 0),
                          // color: UiStyle.backgroundBottomNavigationOthersColor,
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.solidImages,
                              size: 25,
                              color: model.fifthTabColor(model.pageNo),
                            ),
                          ),
                        ),
                      ),
                      FlipCard(
                        key: model.cardKey5,
                        direction: FlipDirection.VERTICAL,
                        front: BottomTapBarMobileLayout(
                          onTap: () {
                            model.onTab(5, 0);
                            model.toggleCard(5);
                          },
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.child,
                              size: 25,
                              color: model.sixTabColor(model.pageNo),
                            ),
                            color: UiStyle.childSelectionColor,
                          ),
                        ),
                        back: BottomTapBarMobileLayout(
                          onTap: () {
                            model.onTab(5, 0);
                            model.toggleCard(5);
                          },
                          child: BottomNavigationItemMobile(
                            icons: Icon(
                              FontAwesomeIcons.music,
                              size: 25,
                              color: model.sixTabColor(model.pageNo),
                            ),
                            color:
                                UiStyle.backgroundBottomNavigationOthersColor,
                          ),
                        ),
                      ),
                      BottomTapBarMobileLayout(
                        onTap: () => model.onTab(6, 0),
                        child: BottomNavigationItemMobile(
                          icons: Icon(
                            FontAwesomeIcons.bars,
                            size: 25,
                            color: model.sevenTabColor(model.pageNo),
                          ),
                          color: UiStyle.backgroundBottomNavigationOthersColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget pageList(StartupViewModel model) {
    if (model.isChangeBottomItem == false) {
      switch (model.pageNo) {
        case 0:
          return HomeView(
            startupViewModel: model,
          );
        case 1:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 2:
          return SermonView(
            startupViewModel: model,
          );
        case 3:
          return EventView(
            startupViewModel: model,
          );
        case 4:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 5:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 6:
          return ChurchInfoView(
            startupViewModel: model,
          );
      }
    } else {
      switch (model.pageNo) {
        case 0:
          return HomeView(
            startupViewModel: model,
          );
        case 1:
          return test(
              //startupViewModel: model,
              );
        case 2:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 3:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 4:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 5:
          return ChurchInfoView(
            startupViewModel: model,
          );
        case 6:
          return ChurchInfoView(
            startupViewModel: model,
          );
      }
    }
  }
}
