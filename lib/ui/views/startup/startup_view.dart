import 'package:doh_homepage_app/app/router.gr.dart';
import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_mobile/startup_mobile_landscape.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_mobile/startup_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class StartupView extends StatelessWidget {
  const StartupView({Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size sizeInformation = MediaQuery.of(context).size;
    UiStyle().init(sizeInformation, context);

    return ScreenTypeLayout(
        mobile: OrientationLayout(
          portrait: StartupMobilePortrait(),
          landscape: StartupMobileLandscape(),
        ),
       /* tablet: OrientationLayout(
          portrait: HomeTabletPortrait(),
          landscape: HomeTabletLandscape(),
        ),
        desktop:OrientationLayout(
          portrait: HomeTabletPortrait(),
          landscape: HomeTabletLandscape(),
        ),*/
      );
  }
}