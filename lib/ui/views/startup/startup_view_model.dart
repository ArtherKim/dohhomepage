import 'dart:async';

import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked/stacked.dart';

@lazySingleton
class StartupViewModel extends BaseViewModel{
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  ScrollController _scrollController;
  get scrollerController => _scrollController;

  GlobalKey<FlipCardState> _cardKey1 = GlobalKey<FlipCardState>();
  get cardKey1 => _cardKey1;

  GlobalKey<FlipCardState> _cardKey2 = GlobalKey<FlipCardState>();
  get cardKey2 => _cardKey2;

  GlobalKey<FlipCardState> _cardKey3 = GlobalKey<FlipCardState>();
  get cardKey3 => _cardKey3;

  GlobalKey<FlipCardState> _cardKey4 = GlobalKey<FlipCardState>();
  get cardKey4 => _cardKey4;

  GlobalKey<FlipCardState> _cardKey5 = GlobalKey<FlipCardState>();
  get cardKey5 => _cardKey5;

  int _tabNo = 0;
  get tabNo => _tabNo;

  int _pageNo = 0;
  get pageNo => _pageNo;

  bool _reversePage = false;
  get reversePage => _reversePage;

  bool _isChangeBottomItem = false;
  get isChangeBottomItem => _isChangeBottomItem;

  onModelReady() async{
    await _FCMReady();
    _scrollController = ScrollController();
    notifyListeners();
  }

  void scrollAnimated(){
    _scrollController.animateTo(
        0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300));
    notifyListeners();
  }
  
  _FCMReady() async{
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message : $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch : $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume : $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
        sound: true, badge: true, alert: true, provisional: true
      )
    );
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print(settings.toString());
    });
    _firebaseMessaging.getToken().then((String token){
      assert(token != null);
      print(token.toString());
    });
  }

  Color firstTabColor(int index){
    if(index==0){
      return UiStyle.activeBottomNavigationHomeColor;
    }
    return Colors.white;
  }

  Color secondTabColor(int index){
    if(_isChangeBottomItem == false) {
      if(index == 1){
        return UiStyle.activeBottomNavigationOthersColor;
      }
    } else {
      if(index == 1){
        return UiStyle.activeBottomNavigationChildColor;
      }
    }
    return Colors.white;
  }

  Color thirdTabColor(int index){
    if(index == 2){
      return UiStyle.activeBottomNavigationOthersColor;
    }
    return Colors.white;
  }

  Color fourthTabColor(int index){
    if(index == 3){
      return UiStyle.activeBottomNavigationOthersColor;
    }
    return Colors.white;
  }

  Color fifthTabColor(int index){
    if(index == 4){
      return UiStyle.activeBottomNavigationOthersColor;
    }
    return Colors.white;
  }
  Color sixTabColor(int index){
    if(_isChangeBottomItem == false) {
      if(index == 5){
        return UiStyle.activeBottomNavigationChildColor;
      }
    } else {
      if(index == 5){
        return UiStyle.activeBottomNavigationOthersColor;
      }
    }
    return Colors.white;
  }
  Color sevenTabColor(int index){
    if(index == 6){
      return UiStyle.activeBottomNavigationOthersColor;
    }
    return Colors.white;
  }

  void toggleCard(int index){
    if(index == 5 && _isChangeBottomItem == false) {
      _isChangeBottomItem = true;
      _pageNo = 1;
      _cardKey1.currentState.toggleCard();
      Timer(Duration(milliseconds: 100), (){
        _cardKey2.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 200), (){
        _cardKey3.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 300), (){
        _cardKey4.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 400), (){
        _cardKey5.currentState.toggleCard();
      });
    }
    if(index == 0 && _isChangeBottomItem == true) {
      _isChangeBottomItem = false;
      _pageNo = 0;
      _cardKey1.currentState.toggleCard();
      Timer(Duration(milliseconds: 100), (){
        _cardKey2.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 200), (){
        _cardKey3.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 300), (){
        _cardKey4.currentState.toggleCard();
      });
      Timer(Duration(milliseconds: 400), (){
        _cardKey5.currentState.toggleCard();
      });
    }
    notifyListeners();
  }

  void onTab(int index, int tabNo,){
    if(index < _pageNo) {
      _reversePage = true;
    } else {
      _reversePage = false;
    }
    _pageNo = index;
    _tabNo = tabNo;
    notifyListeners();
  }
}