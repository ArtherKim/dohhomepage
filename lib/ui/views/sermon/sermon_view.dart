import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_portrait.dart';
import 'package:doh_homepage_app/ui/views/sermon/sermon_mobile/sermon_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';

class SermonView extends StatelessWidget {
  final StartupViewModel startupViewModel;
  const SermonView({Key key, this.startupViewModel,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: SermonMobilePortrait(startupViewModel: startupViewModel,),
        landscape: Container(child: Text('aaaaa'),)//SermonMobilePortrait(startupViewModel: startupViewModel,),
      ),
     /* tablet: OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),
      desktop:OrientationLayout(
        portrait: HomeTabletPortrait(),
        landscape: HomeTabletLandscape(),
      ),*/
    );
  }
}