import 'dart:convert';
import 'dart:core';

import 'package:doh_homepage_app/model/posts/base_req.dart';
import 'package:doh_homepage_app/model/posts/sermons/sermons_res.dart';
import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SermonCustomModel {
  final String month;
  final String day;
  final String title;
  final String chapter;
  final String date;
  final String preacher;
  final String years;
  final String url;
  num turns;
  bool isShow;

  SermonCustomModel(
      {this.month,this.turns,
        this.day,
        this.url,
        this.years,
        this.title,
      this.chapter,
      this.date,
      this.preacher,
      this.isShow});
}

class SermonViewModel extends CustomRootModel {
  int _paged = 1;

  List<SermonsRes> _sermonsRes = [];
  get sermonRes => _sermonsRes;

  List<SermonCustomModel> _sermonCustomModel = [];
  get sermonCustomModel => _sermonCustomModel;

  ScrollController _scrCtr = ScrollController();
  get scrCtr => _scrCtr;

  bool _isGetMore = false;
  get isGetMore => _isGetMore;

  bool _isEmpty = false;
  get isEmpty => _isEmpty;


  onModelReady() async{
    pr.show();
    await getDataFromServer();
  }

  getDataFromServer() async{
    BaseReq baseReq = BaseReq(
      action: "load_posts_by_ajax",
      postType: "post",
      paged: _paged,
      postPerPage: 10,
      categoryName: "sermon",
      metaKey: "sermon_date",
      orderBy: "meta_value",
      order: "DESC",
      security: storageUtil.securityCode,
    );
    if(_isGetMore == false) changeIsGetMore(true);
    restAPIHelper.fetchPost(baseReq, url: restAPIHelper.wpAjax).then((value) async{
      Map valueMap = json.decode(value.data);
      List sermonList = [
        for (int i = 0; i < valueMap.length; i++)
          SermonsRes.fromJson(valueMap["$i"]),
      ].toList();
      _sermonsRes = sermonList;
      await _inputData();
      _paged ++;
      notifyListeners();
    }).catchError((e) async {
      if (pr.isShowing) pr.dismiss();
      if(_isGetMore == true) changeIsGetMore(false);
      changeIsEmpty(true);
    }).whenComplete(() {
      if(_isGetMore == true) changeIsGetMore(false);
      if (pr.isShowing) pr.dismiss();
    });
    notifyListeners();
  }

  _inputData() async{
    for(int i =0; i<_sermonsRes.length; i++)
    _sermonCustomModel.add(
      SermonCustomModel(
        turns: 1,
        years: DateFormat("yyyy-MM-dd").parse(_sermonsRes[i].sermonDate).year.toString(),
        month: DateFormat("yyyy-MM-dd").parse(_sermonsRes[i].sermonDate).month.toString(),
        day: DateFormat("yyyy-MM-dd").parse(_sermonsRes[i].sermonDate).day.toString(),
        title: _sermonsRes[i].sermonTitle,
        chapter: _sermonsRes[i].sermonChapter,
        date: _sermonsRes[i].sermonDate,
        preacher: _sermonsRes[i].sermonPreacher,
        url: _sermonsRes[i].sermonVideoLink,
        isShow: false,
      ),
    );
    notifyListeners();
  }

  void changeIsShow(i){
    if(_sermonCustomModel[i].isShow == false){
      _sermonCustomModel[i].isShow = true;
      _sermonCustomModel[i].turns = 3;
    } else {
      _sermonCustomModel[i].isShow = false;
      _sermonCustomModel[i].turns = 1;
    }
    notifyListeners();
  }

  changeIsGetMore(bool value) async{
    _isGetMore = value;
    notifyListeners();
  }

  void changeIsEmpty(bool value){
    _isEmpty = value;
    notifyListeners();
  }

  void delayPagedChange(){
    if(_paged == 1) {
      changeIsGetMore(true);
      Future.delayed(Duration(milliseconds: 500),(){
        delayPagedChange();
      });
    } else {
      getDataFromServer();
    }
    notifyListeners();
  }
}
