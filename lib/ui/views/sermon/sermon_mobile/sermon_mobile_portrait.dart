import 'dart:math';

import 'package:doh_homepage_app/ui/views/sermon/sermon_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/ui/views/test.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/app_bar/app_bar_widget.dart';
import 'package:doh_homepage_app/widgets/custom_sermon_box/custom_sermon_box_widget.dart';
import 'package:doh_homepage_app/widgets/responsive_text.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:swipedetector/swipedetector.dart';

class SermonMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;

  const SermonMobilePortrait({Key key, this.startupViewModel})
      : super(key: key);

  @override
  _SermonMobilePortraitState createState() => _SermonMobilePortraitState();
}

class _SermonMobilePortraitState extends State<SermonMobilePortrait> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SermonViewModel>.reactive(
        viewModelBuilder: () => SermonViewModel(),
        onModelReady: (model) => onModelInit(model),
        builder: (context, model, child) => Scaffold(
              appBar: PreferredSize(
                preferredSize:
                    Size.fromHeight(UiStyle.blockSizeHorizontal * 20),
                child: AppBarLayout(
                  title: '설교',
                  icon: FontAwesomeIcons.bible,
                  backgroundColor: Color(0xffe08484),
                  button1: TailButtonLayout(
                    width: UiStyle.blockSizeHorizontal * 18,
                    title: '날짜순',
                    backgroundColor: Color(0xff9e5e5e),
                    onTap: () => print('aa'),
                  ),
                  button2: TailButtonLayout(
                    width: UiStyle.blockSizeHorizontal * 18,
                    title: '성경순',
                    backgroundColor: Color(0xff9e5e5e),
                  ),
                ),
              ),
              body: SwipeDetector(
                onSwipeLeft: (){
                  widget.startupViewModel.onTab(3, 0);
                },
                onSwipeRight: (){
                  widget.startupViewModel.onTab(1, 0);
                },
                child: FlipCard(
                  flipOnTouch: false,
                  front: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (!model.isGetMore &&
                          scrollInfo.metrics.pixels ==
                              scrollInfo.metrics.maxScrollExtent) {
                        model.getDataFromServer();
                      }
                      return true;
                    },
                    child: ListView(
                      physics: ClampingScrollPhysics(),
                        children: [for (int i = 0; i < model.sermonCustomModel.length; i++)
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                model.changeIsShow(i);
                              },
                              child: CustomSermonBoxLayout(
                                month: model.sermonCustomModel[i].month,
                                day: model.sermonCustomModel[i].day,
                                sermonTitle: model.sermonCustomModel[i].title,
                                sermonChapter: model.sermonCustomModel[i].chapter,
                                turns: model.sermonCustomModel[i].turns,
                                sermonDate: model.sermonCustomModel[i].date,
                                sermonPreacher:
                                    model.sermonCustomModel[i].preacher,
                                isShow: model.sermonCustomModel[i].isShow,
                                onTap: () {
                                  model.urlLLauncherModel.launcher(model
                                      .urlLLauncherModel
                                      .launchInWebViewWithJavaScript(
                                          model.sermonCustomModel[i].url));
                                },
                              ),
                            ),
                            model.sermonCustomModel[i].years !=
                                    model
                                        .sermonCustomModel[i + 1 ==
                                                model.sermonCustomModel.length
                                            ? i
                                            : i + 1]
                                        .years
                                ? Container(
                                    height: UiStyle.blockSizeHorizontal * 12,
                                    width: UiStyle.width,
                                    color: const Color(0xFF78b6e4),
                                    child: Center(
                                      child: ResponsiveTextWidget(
                                        width: UiStyle.blockSizeHorizontal * 15,
                                        height: UiStyle.blockSizeHorizontal * 6,
                                        fontColor: Colors.white,
                                        fontWeight: FontWeight.w500,
                                        title: (int.parse(model
                                                        .sermonCustomModel[i]
                                                        .years) -
                                                    1)
                                                .toString() +
                                            '년',
                                      ),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      model.isEmpty
                          ? Container(
                              height: 50,
                              color: Colors.transparent,
                              child: Center(
                                child: Text('해당설교가 없습니다'),
                              ),
                            )
                          : Container(
                              height: model.isGetMore ? 60 : 0,
                              color: Colors.transparent,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            )
                    ]),
                  ),
                  back: Container(),
                ),
              ),
            ));
  }

  onModelInit(SermonViewModel model) async {
    await model.prReady(context);
    await model.onModelReady();
    model.delayPagedChange();
  }
}
