import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_portrait.dart';
import 'package:doh_homepage_app/ui/views/home/home_mobile/home_mobile_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_landscape.dart';
import 'package:doh_homepage_app/ui/views/home/home_tablet/home_tablet_portrait.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  final StartupViewModel startupViewModel;
  const HomeView({Key key, this.startupViewModel,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size sizeInformation = MediaQuery.of(context).size;
    UiStyle().init(sizeInformation, context);

    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: HomeMobilePortrait(startupViewModel: startupViewModel,),
        landscape: HomeMobileLandscape(startupViewModel: startupViewModel),
      ),
      // tablet: OrientationLayout(
      //   portrait: HomeTabletPortrait(),
      //   landscape: HomeTabletLandscape(),
      // ),
      // desktop:OrientationLayout(
      //   portrait: HomeTabletPortrait(),
      //   landscape: HomeTabletLandscape(),
      // ),
    );
  }
}
