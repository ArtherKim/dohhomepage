import 'dart:convert';
import 'package:doh_homepage_app/model/home/home_res.dart';
import 'package:doh_homepage_app/model/posts/sermons/sermons_res.dart';
import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:doh_homepage_app/ui/luncher_function_model.dart';

class HomeViewModel extends CustomRootModel{

  HomeRes _homeRes;
  get homeRes => _homeRes;

  SermonsRes _sermonsRes = SermonsRes(
    sermonTitle:'',
    sermonChapter:'',
    sermonVideoLink:'https://www.youtube.com/channel/UCDCOZ-fH7FUbv9p2EL2HveQ',
    sermonDate:'',
    sermonPreacher:'',
  );
  get sermonRes => _sermonsRes;

  onModelReady(){
    ///내부저장소에 sermon 추가
    ///내부저장소에 데이터가 있을경우 로딩을 하지않음...
    ///갱신주기..?
    getDataFromServer();
  }

  getDataFromServer(){
    pr.show();
    restAPIHelper.fetchGet('', url: restAPIHelper.firstUrl).then((value){
      var convertToJson = json.decode(value.data);
      _homeRes = HomeRes.fromJson(convertToJson);
      storageUtil.setSecurityCode(_homeRes.securityCode);
      storageUtil.setAjaxLoginNonce(_homeRes.loginCode);
      _inputData(_homeRes.sermon);
      if(pr.isShowing) pr.dismiss();
    }).catchError((e){
      if(pr.isShowing) pr.dismiss();
    });
    notifyListeners();
  }

  _inputData(SermonsRes sermonRes){
    _sermonsRes = sermonRes;
    notifyListeners();
  }
}