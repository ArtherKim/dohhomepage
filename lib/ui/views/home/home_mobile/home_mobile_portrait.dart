import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/enums/dialog_type.dart';
import 'package:doh_homepage_app/ui/views/home/home_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/ui/views/test.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/responsive_text.dart';
import 'package:doh_homepage_app/widgets/tile/tile_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:swipedetector/swipedetector.dart';

class HomeMobilePortrait extends StatefulWidget {
  final StartupViewModel startupViewModel;

  const HomeMobilePortrait({Key key, this.startupViewModel}) : super(key: key);

  @override
  _HomeMobilePortraitState createState() => _HomeMobilePortraitState();
}

class _HomeMobilePortraitState extends State<HomeMobilePortrait> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => onModelInit(model),
      builder: (context, model, child) => Scaffold(
        body: SwipeDetector(
          onSwipeLeft: () {
            model.swipePage(1, 0, widget.startupViewModel);
          },
          child: ListView(
            padding: const EdgeInsets.all(0),
            physics: ClampingScrollPhysics(),
            controller: widget.startupViewModel.scrollerController,
            children: [
              Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        height: UiStyle.blockSizeHorizontal * 11,
                        color: Colors.white,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                          color: Colors.white,
                          width: 2,
                        ))),
                        child: Stack(
                          children: [
                            SizedBox(
                              height: UiStyle.blockSizeHorizontal * 39,
                              width: UiStyle.width,
                              child: Image.asset(
                                  'img/home_view_img/main-visual1-1024x329.jpg',
                                  fit: BoxFit.cover),
                            ),
                            Positioned(
                                top: UiStyle.blockSizeHorizontal * 8,
                                left: UiStyle.blockSizeHorizontal * 43,
                                child: ResponsiveTextWidget(
                                  width: UiStyle.blockSizeHorizontal * 40,
                                  height: UiStyle.blockSizeHorizontal * 4,
                                  title: '우리가 예수님을 통해 하나님을 본 것처럼,',
                                  fontColor: UiStyle.videoPlayerBackgroundColor,
                                  fontWeight: FontWeight.bold,
                                )),
                            Positioned(
                                top: UiStyle.blockSizeHorizontal * 12.5,
                                left: UiStyle.blockSizeHorizontal * 52,
                                child: ResponsiveTextWidget(
                                  width: UiStyle.blockSizeHorizontal * 30,
                                  height: UiStyle.blockSizeHorizontal * 5,
                                  title: '세상도 우리를 통해',
                                  fontColor: UiStyle.videoPlayerBackgroundColor,
                                  fontWeight: FontWeight.bold,
                                )),
                            Positioned(
                                top: UiStyle.blockSizeHorizontal * 18,
                                left: UiStyle.blockSizeHorizontal * 52,
                                child: ResponsiveTextWidget(
                                  width: UiStyle.blockSizeHorizontal * 37,
                                  height: UiStyle.blockSizeHorizontal * 6,
                                  title: '하나님을 보게 됩니다',
                                  fontColor: UiStyle.videoPlayerBackgroundColor,
                                  fontWeight: FontWeight.bold,
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: SizedBox(
                          height: UiStyle.blockSizeHorizontal * 10,
                          child: Image.asset('img/home_view_img/logo.png',
                              fit: BoxFit.cover)),
                    ),
                  ),
                ],
              ),
              Container(
                width: double.infinity,
                height: UiStyle.blockSizeHorizontal * 40,
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                  color: Colors.white,
                  width: 2,
                ))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: UiStyle.safeBlockHorizontal * 64,
                      color: UiStyle.videoPlayerBackgroundColor,
                      padding: EdgeInsets.fromLTRB(30, 20, 10, 0),
                      child: GestureDetector(
                        onTap: () {
                          widget.startupViewModel.onTab(
                            2,
                            0,
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ResponsiveTextWidget(
                              height: UiStyle.blockSizeHorizontal * 8,
                              width: UiStyle.blockSizeHorizontal * 32,
                              fontWeight: FontWeight.w900,
                              title: '주일설교 듣기',
                              fontColor: Colors.white,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            ResponsiveTextWidget(
                              height: UiStyle.blockSizeHorizontal * 6,
                              width: UiStyle.blockSizeHorizontal * 38,
                              fontWeight: FontWeight.w600,
                              title: model.sermonRes.sermonTitle,
                              fontColor: Colors.white,
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            ResponsiveTextWidget(
                              height: UiStyle.blockSizeHorizontal * 4,
                              width: UiStyle.blockSizeHorizontal * 30,
                              fontWeight: FontWeight.w600,
                              title: model.sermonRes.sermonChapter,
                              fontColor: Colors.white,
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            ResponsiveTextWidget(
                              height: UiStyle.blockSizeHorizontal * 4,
                              width: UiStyle.blockSizeHorizontal * 18,
                              fontWeight: FontWeight.w400,
                              title: model.sermonRes.sermonDate,
                              fontColor: Colors.white.withOpacity(0.7),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: ResponsiveTextWidget(
                                height: UiStyle.blockSizeHorizontal * 5,
                                width: UiStyle.blockSizeHorizontal * 18,
                                fontWeight: FontWeight.w300,
                                title: model.sermonRes.sermonPreacher,
                                fontColor: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Stack(
                      children: [
                        Image.asset(
                          'img/home_view_img/video_player_background.jpg',
                          fit: BoxFit.fill,
                          height: UiStyle.safeBlockHorizontal * 40,
                          width: UiStyle.safeBlockHorizontal * 36,
                        ),
                        GestureDetector(
                          onTap: () {
                            model.urlLLauncherModel.launcher(model
                                .urlLLauncherModel
                                .launchInWebViewWithJavaScript(
                                    model.sermonRes.sermonVideoLink));
                          },
                          child: Center(
                            child: Container(
                              height: UiStyle.blockSizeHorizontal * 36,
                              width: UiStyle.blockSizeHorizontal * 36,
                              child: Image.asset(
                                'img/home_view_img/play-botton.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: UiStyle.width,
                height: UiStyle.blockSizeHorizontal * 70 + 2,
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                  color: Colors.white,
                  width: 2,
                ))),
                child: Stack(
                  children: [
                    SizedBox(
                      width: UiStyle.width,
                      child: Image.asset(
                        'img/home_view_img/bible_tea_612.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  0,
                                );
                              },
                              icon: FontAwesomeIcons.church, //Icons.print,
                              iconColor: Colors.white,
                              title: '교회소식',
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  1,
                                );
                              },
                              icon: FontAwesomeIcons.fish,
                              title: '교회생활',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  2,
                                );
                              },
                              icon: FontAwesomeIcons.graduationCap,
                              title: '교육훈련',
                              iconColor: Colors.white,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  3,
                                  0,
                                );
                              },
                              icon: FontAwesomeIcons.calendarAlt,
                              title: '일정',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  4,
                                  0,
                                );
                              },
                              icon: FontAwesomeIcons.images,
                              title: '사진',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(5, 0);
                                widget.startupViewModel.toggleCard(5);
                              },
                              icon: FontAwesomeIcons.child,
                              title: '주일학교',
                              iconColor: UiStyle.childSelectionColor,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: UiStyle.safeBlockVertical * 50,
                width: UiStyle.safeBlockHorizontal * 100,
                color: Colors.blueGrey,
              )

              /// add: 요리문답?
            ],
          ),
        ),
      ),
    );
  }

  onModelInit(HomeViewModel model) async {
    await model.prReady(context);
    model.onModelReady();
  }
}
