import 'package:doh_homepage_app/ui/views/home/home_view_model.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view_model.dart';
import 'package:doh_homepage_app/ui/views/test.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/tile/tile_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:swipedetector/swipedetector.dart';

class HomeMobileLandscape extends StatefulWidget {
  final StartupViewModel startupViewModel;
  const HomeMobileLandscape({Key key, this.startupViewModel}) : super(key: key);

  @override
  _HomeMobileLandscapeState createState() => _HomeMobileLandscapeState();
}

class _HomeMobileLandscapeState extends State<HomeMobileLandscape>
    with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      builder: (context, model, child) => Scaffold(
        body: SwipeDetector(
          onSwipeLeft: () {
            model.swipePage(1, 0, widget.startupViewModel);
          },
          child: ListView(
            controller: widget.startupViewModel.scrollerController,
            children: [
              Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        height: UiStyle.safeBlockVertical * 10,
                        color: Colors.white,
                      ),
                      Stack(
                        children: [
                          SizedBox(
                              height: UiStyle.safeBlockVertical * 20,
                              width: UiStyle.safeBlockHorizontal * 100,
                              child: Image.asset(
                                  'img/home_view_img/main-visual1-1024x329.jpg',
                                  fit: BoxFit.cover),),
                          Positioned(
                              top: UiStyle.safeBlockVertical * 5,
                              right: UiStyle.safeBlockHorizontal * 20,
                              child: Text(
                                '우리가 예수님을 통해 하나님을 본 것처럼,',
                                style: TextStyle(
                                    fontSize: 8,
                                    color: UiStyle.videoPlayerBackgroundColor,
                                    fontWeight: FontWeight.bold),
                              )),
                          Positioned(
                              top: UiStyle.safeBlockVertical * 7,
                              right: UiStyle.safeBlockHorizontal * 20,
                              child: Text(
                                '세상도 우리를 통해',
                                style: TextStyle(
                                    fontSize: 13,
                                    color: UiStyle.videoPlayerBackgroundColor,
                                    fontWeight: FontWeight.bold),
                              )),
                          Positioned(
                            top: UiStyle.safeBlockVertical * 10,
                            right: UiStyle.safeBlockHorizontal * 10,
                            child: Text(
                              '하나님을 보게 됩니다',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: UiStyle.videoPlayerBackgroundColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: SizedBox(
                          height: UiStyle.safeBlockVertical * 6,
                          child: Image.asset('img/home_view_img/logo.png',
                              fit: BoxFit.cover)),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: UiStyle.safeBlockHorizontal * 65,
                      height: UiStyle.safeBlockVertical * 20,
                      color: UiStyle.videoPlayerBackgroundColor,
                      child: Stack(
                        children: [
                          Positioned(
                            top: UiStyle.safeBlockVertical * 3,
                            left: UiStyle.safeBlockHorizontal * 8,
                            child: RichText(
                              text: TextSpan(
                                  text: '주일설교 듣기\n',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20,
                                      color: Colors.white),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: '다시 복음 앞으로 \n',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 15,
                                          color: Colors.white),
                                    ),
                                    TextSpan(
                                      text: '고린도전서 15장 50~58절 \n',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 13,
                                          color: Colors.white),
                                    ),
                                    TextSpan(
                                      text: '2020-06-28 \n',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 9,
                                          color: Colors.white),
                                    ),
                                  ]),
                            ),
                          ),
                          Positioned(
                              top: UiStyle.safeBlockVertical * 16,
                              right: UiStyle.safeBlockHorizontal * 5,
                              child: Text(
                                '인진우 목사',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: Colors.white),
                              )),
                        ],
                      ),
                    ),
                    Stack(
                      children: [
                        SizedBox(
                          width: UiStyle.safeBlockHorizontal * 35,
                          height: UiStyle.safeBlockVertical * 20,
                          child: Image.asset(
                              'img/home_view_img/video_player_background.jpg',
                              fit: BoxFit.cover),
                        ),
                        GestureDetector(
                          onTap: (){
                          },//model.showCustomVideoDialog,
                          child: SizedBox(
                            width: UiStyle.safeBlockHorizontal * 35,
                            height: UiStyle.safeBlockVertical * 20,
                            child: Image.asset(
                                'img/home_view_img/play-botton.png',
                                fit: BoxFit.cover),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                child: Stack(
                  children: [
                    SizedBox(
                        width: UiStyle.safeBlockHorizontal * 100,
                        height: UiStyle.safeBlockVertical * 36,
                        child: Image.asset(
                          'img/home_view_img/bible_tea_612.jpg',
                          fit: BoxFit.cover,
                        )),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  0,
                                );
                              },
                              icon: FontAwesomeIcons.church, //Icons.print,
                              iconColor: Colors.white,
                              title: '교회소식',
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  1,
                                );
                              },
                              icon: FontAwesomeIcons.fish,
                              title: '교회생활',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              onTap: () {
                                widget.startupViewModel.onTab(
                                  1,
                                  2,
                                );
                              },
                              icon: FontAwesomeIcons.graduationCap,
                              title: '교육훈련',
                              iconColor: Colors.white,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TileWidget(
                              onTap: (){
                                //Navigator.push(context, MaterialPageRoute(builder: (context)=>test()));
                              },
                              icon: FontAwesomeIcons.calendarAlt,
                              title: '일정',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              icon: FontAwesomeIcons.images,
                              title: '사진',
                              iconColor: Colors.white,
                            ),
                            TileWidget(
                              onTap: (){
                                widget.startupViewModel.onTab(5, 0);
                                widget.startupViewModel.toggleCard(5);
                              },
                              icon: FontAwesomeIcons.child,
                              title: '주일학교',
                              iconColor: UiStyle.childSelectionColor,
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                height: UiStyle.safeBlockVertical * 50,
                width: UiStyle.safeBlockHorizontal * 100,
                color: Colors.blueGrey,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
