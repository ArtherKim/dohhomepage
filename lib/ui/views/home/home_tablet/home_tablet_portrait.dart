import 'package:flutter/material.dart';

class HomeTabletPortrait extends StatelessWidget {
  const HomeTabletPortrait({Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text('Tablet_Portrait', style: TextStyle(fontSize: 30),),
        )
    );
  }
}