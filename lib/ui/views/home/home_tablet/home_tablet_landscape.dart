import 'package:flutter/material.dart';

class HomeTabletLandscape extends StatelessWidget {
  const HomeTabletLandscape({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text('Tablet_Landscape', style: TextStyle(fontSize: 30),),
        )
    );
  }
}