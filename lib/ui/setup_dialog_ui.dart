import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/enums/dialog_type.dart';
import 'package:doh_homepage_app/widgets/dialog/date_time_picker.dart';
import 'package:stacked_services/stacked_services.dart';

void setupDialogUi() {
  final dialogService = locator<DialogService>();

  final builders = {
    DialogType.dateTimePicker: (context, sheetRequest, completer) =>
        DateTimePicker(request: sheetRequest, completer: completer),
  };

  dialogService.registerCustomDialogBuilders(builders);
}
