class RestApiResponse<T> {
  Status code;
  String message;
  T data;

  RestApiResponse.http(this.message, this.data) : code = Status.HTTP;
  RestApiResponse.error(this.message, this.data) : code = Status.ERROR;
  RestApiResponse.success(this.message, this.data) : code = Status.SUCCESS;
  RestApiResponse.valid(this.message, this.data) : code = Status.VALID;

  @override
  String toString() {
    return "code : $code \n Message : $message \n Data : $data";
  }
}

enum Status { HTTP, SUCCESS, ERROR, VALID }
