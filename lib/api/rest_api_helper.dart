import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:doh_homepage_app/api/rest_api_response.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class RestAPIHelper {
  final Dio dio = Dio();
  final String prefixURL = "https://drawingheaven.org";
  final String wpAjax = "/wp-admin/admin-ajax.php";
  final String firstUrl = "/codeTest/";

  Future<Response> fetchGet(dynamic _req,{@required String url}) async {
    Response response;

    try{
      if(_req != null && _req != '') {
        response = await dio.get(prefixURL+url, queryParameters: _req);
      } else {
        response = await dio.get(prefixURL+url);
      }
    } on DioError catch (e) {
      print(e);
    }
    return response;
  }

  Future<Response> fetchPost(dynamic _req, {@required String url}) async {
    Response response;
    try {
      if (_req != null && _req != '') {
        FormData formData = new FormData.fromMap(_req.toJson());
        response = await dio.post(prefixURL+url, data: formData);
      } else {
        response = await dio.post(prefixURL+url);
      }
    } on DioError catch (e) {
      print(e);
    }
    return response;
  }

  // RestApiResponse _returnResponse(Response<dynamic> response) {
  //
  // }
}
  //   var statusCode = response.statusCode;
  //   BaseRes _baseRes;
  //
  //   try {
  //     Map<String, dynamic> responseData = response.data;
  //     _baseRes = BaseRes.fromJson(response.data);
  //
  //     var msgFromServer = _baseRes.message;
  //
  //     switch (response.statusCode) {
  //       case 200:
  //       case 203:
  //         if (responseData.containsKey('type') == true) {
  //           if (_baseRes.type == 'success')
  //             return RestApiResponse.success('success', responseData);
  //           else
  //             return RestApiResponse.error(_baseRes.type, responseData);
  //         } else {
  //           return RestApiResponse.success('success', responseData);
  //         }
  //         break;
  //       case 422:
  //         return RestApiResponse.error(
  //             'UnprocessableException : $msgFromServer', responseData);
  //       case 400:
  //         if (msgFromServer.contains('token_expired') ||
  //             msgFromServer.contains('token_invalid')) {
  //           return RestApiResponse.valid(
  //               'BadRequestException : $msgFromServer', responseData);
  //         } else {
  //           return RestApiResponse.http(
  //               'BadRequestException : $msgFromServer', responseData);
  //         }
  //         break;
  //       case 401:
  //       case 403:
  //         return RestApiResponse.http(
  //             'UnauthorisedException : $msgFromServer', responseData);
  //       case 500:
  //       default:
  //         return RestApiResponse.http(
  //             'Error occured while Communication with Server with StatusCode : ${statusCode.toString()}',
  //             responseData);
  //     }
  //   } catch (e) {
  //     logger.d(e);
  //
  //     return RestApiResponse.error(
  //         'Error occured while parsing data : ${statusCode.toString()}',
  //         response.data);
  //   }
  // }