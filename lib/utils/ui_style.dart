import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class UiStyle {
  static double width;
  static double height;
  static int initCount = 1;

  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  static Color videoPlayerBackgroundColor;
  static Color activeBottomNavigationHomeColor;
  static Color backgroundBottomNavigationHomeColor;
  static Color backgroundBottomNavigationOthersColor;
  static Color activeBottomNavigationOthersColor;
  static Color childSelectionColor;
  static Color activeBottomNavigationChildColor;
  static Color activeTabColor;

  void init(Size size, BuildContext context) {
    width = size.width;
    height = size.height;
    initCount++;

    _mediaQueryData = MediaQuery.of(context);

    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;

    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal = _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;

    videoPlayerBackgroundColor = const Color(0xFF10323E);
    activeBottomNavigationHomeColor = const Color(0xFF2d78af);
    backgroundBottomNavigationHomeColor = const Color(0xFF78b6e4);
    backgroundBottomNavigationOthersColor = const Color(0xFFc7d053);
    activeBottomNavigationOthersColor = const Color(0xFF889e14);
    childSelectionColor = const Color(0xFFFFD400);
    activeBottomNavigationChildColor = const Color(0xFFcaa800);
    activeTabColor = const Color(0xFF6fc8d2);
  }
}