// import 'package:doh_homepage_app/model/posts/sermons/sermons_res.dart';
// import 'package:json_annotation/json_annotation.dart';
// import 'package:onesignal_flutter/onesignal_flutter.dart';
//
// part 'push_notification_res.g.dart';
//
// @JsonSerializable()
// class PushNotificationRes {
//   PushNotificationRes({
//     this.securityCode,
//     this.loginCode,
//     this.sermon,
//   });
//
//   @JsonKey(name: "security_code")
//   String securityCode;
//   @JsonKey(name: "ajax_login_nonce")
//   String loginCode;
//   SermonsRes sermon;
//
//   initPlatformState() async {
//     String _oneSignalAppId = 'f1b0bcfe-048c-431f-8f6e-6a11b0071dc1';
//     OneSignal.shared.init(
//       _oneSignalAppId,
//     );
//     OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
//
//     OneSignal.shared.setNotificationOpenedHandler((openedResult) {
//       var data = openedResult.notification.payload.body;
//
//       //notifyListeners();
//     });
//   }
//
//   factory PushNotificationRes.fromJson(Map<String, dynamic> json) => _$PushNotificationResFromJson(json);
//   Map<String, dynamic> toJson() => _$PushNotificationResToJson(this);
// }
