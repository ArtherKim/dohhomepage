import 'package:json_annotation/json_annotation.dart';

part 'sermons_res.g.dart';

@JsonSerializable()
class SermonsRes {
  SermonsRes({
    this.postId,
    this.postTitle,
    this.sermonTitle,
    this.sermonChapter,
    this.sermonVideoLink,
    this.sermonDate,
    this.sermonPreacher});

  @JsonKey(name: "post_id")
  int postId;
  @JsonKey(name: "post_title")
  String postTitle;
  @JsonKey(name: "sermon_title")
  String sermonTitle;
  @JsonKey(name: "sermon_chapter")
  String sermonChapter;
  @JsonKey(name: "sermon_video_link")
  String sermonVideoLink;
  @JsonKey(name: "sermon_date")
  String sermonDate;
  @JsonKey(name: "sermon_preacher")
  String sermonPreacher;

  factory SermonsRes.fromJson(Map<String, dynamic> json) => _$SermonsResFromJson(json);
  Map<String, dynamic> toJson() => _$SermonsResToJson(this);
}
