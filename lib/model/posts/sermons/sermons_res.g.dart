// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sermons_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SermonsRes _$SermonsResFromJson(Map<String, dynamic> json) {
  return SermonsRes(
    postId: json['post_id'] as int,
    postTitle: json['post_title'] as String,
    sermonTitle: json['sermon_title'] as String,
    sermonChapter: json['sermon_chapter'] as String,
    sermonVideoLink: json['sermon_video_link'] as String,
    sermonDate: json['sermon_date'] as String,
    sermonPreacher: json['sermon_preacher'] as String,
  );
}

Map<String, dynamic> _$SermonsResToJson(SermonsRes instance) =>
    <String, dynamic>{
      'post_id': instance.postId,
      'post_title': instance.postTitle,
      'sermon_title': instance.sermonTitle,
      'sermon_chapter': instance.sermonChapter,
      'sermon_video_link': instance.sermonVideoLink,
      'sermon_date': instance.sermonDate,
      'sermon_preacher': instance.sermonPreacher,
    };
