// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoryRes _$HistoryResFromJson(Map<String, dynamic> json) {
  return HistoryRes(
    postId: json['post_id'] as int,
    postTitle: json['post_title'] as String,
    about: json['about'] as String,
    dateEnd: json['date_end'] as String,
    content: (json['content'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    dateStart: json['date_start'] as String,
  );
}

Map<String, dynamic> _$HistoryResToJson(HistoryRes instance) =>
    <String, dynamic>{
      'post_id': instance.postId,
      'post_title': instance.postTitle,
      'about': instance.about,
      'date_start': instance.dateStart,
      'date_end': instance.dateEnd,
      'content': instance.content,
    };
