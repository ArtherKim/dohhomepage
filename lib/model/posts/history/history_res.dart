import 'package:json_annotation/json_annotation.dart';

part 'history_res.g.dart';

@JsonSerializable()
class HistoryRes {
  HistoryRes({
    this.postId,
    this.postTitle,
    this.about,
    this.dateEnd,
    this.content,
    this.dateStart,
  });

  @JsonKey(name: "post_id")
  final int postId;
  @JsonKey(name: "post_title")
  String postTitle;
  String about;
  @JsonKey(name: "date_start")
  String dateStart;
  @JsonKey(name: "date_end")
  String dateEnd;
  Map<String,String> content;

  factory HistoryRes.fromJson(Map<String, dynamic> json) => _$HistoryResFromJson(json);
  Map<String, dynamic> toJson() => _$HistoryResToJson(this);
}