import 'package:json_annotation/json_annotation.dart';

part 'base_req.g.dart';

@JsonSerializable()
class BaseReq {
  BaseReq({
    this.action = 'load_posts_by_ajax',
    this.postType = 'post',
    this.pageId,
    this.paged,
    this.postPerPage,
    this.categoryName,
    this.metaKey,
    this.orderBy= 'meta_value',
    this.order = 'DESC',
    this.hasRangeCondition,
    this.metaQueryKey,
    this.startDate,
    this.endDate,
    this.security,
  });

  final String action;
  @JsonKey(name: "post_type")
  final String postType;
  @JsonKey(name: "page_id")
  final String pageId;
  final int paged;
  @JsonKey(name: "posts_per_page")
  final int postPerPage;
  @JsonKey(name: "category_name")
  final String categoryName;
  @JsonKey(name: "meta_key")
  final String metaKey;
  @JsonKey(name: "orderby")
  final String orderBy;
  final String order;
  final bool hasRangeCondition;
  @JsonKey(name: "meta_query_key")
  final String metaQueryKey;
  @JsonKey(name: "start_date")
  final String startDate;
  @JsonKey(name:"end_date")
  final String endDate;
  final String security;

  factory BaseReq.fromJson(Map<String, dynamic> json) => _$BaseReqFromJson(json);
  Map<String, dynamic> toJson() => _$BaseReqToJson(this);
}
