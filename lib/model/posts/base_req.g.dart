// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_req.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseReq _$BaseReqFromJson(Map<String, dynamic> json) {
  return BaseReq(
    action: json['action'] as String,
    postType: json['post_type'] as String,
    pageId: json['page_id'] as String,
    paged: json['paged'] as int,
    postPerPage: json['posts_per_page'] as int,
    categoryName: json['category_name'] as String,
    metaKey: json['meta_key'] as String,
    orderBy: json['orderby'] as String,
    order: json['order'] as String,
    hasRangeCondition: json['hasRangeCondition'] as bool,
    metaQueryKey: json['meta_query_key'] as String,
    startDate: json['start_date'] as String,
    endDate: json['end_date'] as String,
    security: json['security'] as String,
  );
}

Map<String, dynamic> _$BaseReqToJson(BaseReq instance) => <String, dynamic>{
      'action': instance.action,
      'post_type': instance.postType,
      'page_id': instance.pageId,
      'paged': instance.paged,
      'posts_per_page': instance.postPerPage,
      'category_name': instance.categoryName,
      'meta_key': instance.metaKey,
      'orderby': instance.orderBy,
      'order': instance.order,
      'hasRangeCondition': instance.hasRangeCondition,
      'meta_query_key': instance.metaQueryKey,
      'start_date': instance.startDate,
      'end_date': instance.endDate,
      'security': instance.security,
    };
