import 'package:json_annotation/json_annotation.dart';

part 'base_res.g.dart';

@JsonSerializable()
class BaseRes{
  dynamic data;

  BaseRes({this.data});

  factory BaseRes.fromJson(Map<String, dynamic> json) => _$BaseResFromJson(json);
  Map<String, dynamic> toJson() => _$BaseResToJson(this);
}