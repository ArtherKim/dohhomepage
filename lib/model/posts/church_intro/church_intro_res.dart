import 'package:json_annotation/json_annotation.dart';

part 'church_intro_res.g.dart';

@JsonSerializable()
class ChurchIntroRes {
  ChurchIntroRes({
    this.postId,
    this.postTitle,
    this.about,
    this.staff,
  });

  @JsonKey(name: "post_id")
  final int postId;
  @JsonKey(name: "post_title")
  final String postTitle;

  String about;
  final Map<String,StaffRes> staff;

  factory ChurchIntroRes.fromJson(Map<String, dynamic> json) => _$ChurchIntroResFromJson(json);
  Map<String, dynamic> toJson() => _$ChurchIntroResToJson(this);
}

@JsonSerializable()
class StaffRes {
  StaffRes({
    this.title,
    this.name,
    this.email,
    this.cellphone,
    this.kakaoId
  });

  String title;
  String name;
  String email;
  String cellphone;
  @JsonKey(name: "kakao_id")
  String kakaoId;

  factory StaffRes.fromJson(Map<String, dynamic> json) => _$StaffResFromJson(json);
  Map<String, dynamic> toJson() => _$StaffResToJson(this);
}
