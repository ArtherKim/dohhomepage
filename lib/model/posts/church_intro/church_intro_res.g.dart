// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'church_intro_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChurchIntroRes _$ChurchIntroResFromJson(Map<String, dynamic> json) {
  return ChurchIntroRes(
    postId: json['post_id'] as int,
    postTitle: json['post_title'] as String,
    about: json['about'] as String,
    staff: (json['staff'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k, e == null ? null : StaffRes.fromJson(e as Map<String, dynamic>)),
    ),
  );
}

Map<String, dynamic> _$ChurchIntroResToJson(ChurchIntroRes instance) =>
    <String, dynamic>{
      'post_id': instance.postId,
      'post_title': instance.postTitle,
      'about': instance.about,
      'staff': instance.staff,
    };

StaffRes _$StaffResFromJson(Map<String, dynamic> json) {
  return StaffRes(
    title: json['title'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    cellphone: json['cellphone'] as String,
    kakaoId: json['kakao_id'] as String,
  );
}

Map<String, dynamic> _$StaffResToJson(StaffRes instance) => <String, dynamic>{
      'title': instance.title,
      'name': instance.name,
      'email': instance.email,
      'cellphone': instance.cellphone,
      'kakao_id': instance.kakaoId,
    };
