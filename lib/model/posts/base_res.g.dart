// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseRes _$BaseResFromJson(Map<String, dynamic> json) {
  return BaseRes(
    data: json['data'],
  );
}

Map<String, dynamic> _$BaseResToJson(BaseRes instance) => <String, dynamic>{
      'data': instance.data,
    };
