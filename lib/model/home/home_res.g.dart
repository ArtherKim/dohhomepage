// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeRes _$HomeResFromJson(Map<String, dynamic> json) {
  return HomeRes(
    securityCode: json['security_code'] as String,
    loginCode: json['ajax_login_nonce'] as String,
    sermon: json['sermon'] == null
        ? null
        : SermonsRes.fromJson(json['sermon'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$HomeResToJson(HomeRes instance) => <String, dynamic>{
      'security_code': instance.securityCode,
      'ajax_login_nonce': instance.loginCode,
      'sermon': instance.sermon,
    };
