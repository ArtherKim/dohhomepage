import 'package:doh_homepage_app/model/posts/sermons/sermons_res.dart';
import 'package:json_annotation/json_annotation.dart';

part 'home_res.g.dart';

@JsonSerializable()
class HomeRes {
  HomeRes({
    this.securityCode,
    this.loginCode,
    this.sermon,
   });

  @JsonKey(name: "security_code")
  String securityCode;
  @JsonKey(name: "ajax_login_nonce")
  String loginCode;
  SermonsRes sermon;

  factory HomeRes.fromJson(Map<String, dynamic> json) => _$HomeResFromJson(json);
  Map<String, dynamic> toJson() => _$HomeResToJson(this);
}
