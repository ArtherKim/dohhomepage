import 'package:auto_route/auto_route.dart';
import 'package:doh_homepage_app/app/locator.dart';
import 'package:doh_homepage_app/app/router.gr.dart' as customRouter;
import 'package:doh_homepage_app/service/shared_preference.dart';
import 'package:doh_homepage_app/ui/custom_root_model.dart';
import 'package:doh_homepage_app/ui/setup_dialog_ui.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:flutter/services.dart';

void main() {
  setupLocator();
  setupDialogUi();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartModel>.reactive(
      viewModelBuilder: () => StartModel(),
      onModelReady: (model) => model.getStorageInstance(),
      builder: (context,model,child)=> MaterialApp(
        initialRoute: customRouter.Routes.startupView,
        onGenerateRoute: customRouter.Router().onGenerateRoute,
        navigatorKey: locator<NavigationService>().navigatorKey,
      ),
    );
  }
}

class StartModel extends CustomRootModel {

}

