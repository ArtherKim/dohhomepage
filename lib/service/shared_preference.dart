import 'dart:convert';

import 'package:doh_homepage_app/model/posts/church_intro/church_intro_res.dart';
import 'package:doh_homepage_app/model/posts/history/history_res.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class StorageUtil {
  SharedPreferences spf;
  final String _keyChurchIntroData = "churchIntroData";
  final String _keySecurityCode = "security_code";
  final String _keyAjaxLoginNonce = "ajax_login_nonce";
  final String _keyHistoryData = "historyData";
  final String _keySermonData = "sermonData";

  Future getInstance() async{
    spf = await SharedPreferences.getInstance();
  }

  Future removeAllInstance() async {
    await spf.clear();
  }

  dynamic _getFromDisk(String key) {
    var value;
    if(key == _keyHistoryData) {
      value = spf.getStringList(key);
    } else {
      value = spf.getString(key);
    }
    return value;
  }

  void saveStringToDisk(String key, String content) {
    spf.setString(key, content);
  }

  void saveStringListToDisk(String key, List<String> content){
    spf.setStringList(key, content);
  }

  void setChurchIntroData(ChurchIntroRes churchIntroRes) {
    saveStringToDisk(_keyChurchIntroData, jsonEncode(churchIntroRes));
  }

  void setHistoryData(List<HistoryRes> historyRes){
    saveStringListToDisk(_keyHistoryData, [for(int i = 0; i<historyRes.length; i++)jsonEncode(historyRes[i]),]);
  }

  void setSecurityCode(String security){
    saveStringToDisk(_keySecurityCode, security);
  }

  void setAjaxLoginNonce(String ajaxLoginNonce){
    saveStringToDisk(_keyAjaxLoginNonce, ajaxLoginNonce);
  }

  ChurchIntroRes get churchIntro{
    var churchIntroJson = _getFromDisk(_keyChurchIntroData);
    if(churchIntroJson == null){
      return null;
    }

    ChurchIntroRes _churchIntroRes = ChurchIntroRes.fromJson(jsonDecode(churchIntroJson));
    return _churchIntroRes;
  }

  List<HistoryRes> get historyRes{
    var historyJson = _getFromDisk(_keyHistoryData);
    if(historyJson == null){
      return null;
    }

    List<HistoryRes> _historyRes = [for(int i =0; i<historyJson.length; i++) HistoryRes.fromJson(jsonDecode(historyJson[i]))];
    return _historyRes;
  }

  String get securityCode{
    String securityCode = _getFromDisk(_keySecurityCode);
    if(securityCode == null){
      return null;
    }
    return securityCode;
  }

  String get ajaxLoginNonce{
    String ajaxLoginNonce = _getFromDisk(_keyAjaxLoginNonce);
    if(ajaxLoginNonce == null){
      return null;
    }
    return ajaxLoginNonce;
  }
}