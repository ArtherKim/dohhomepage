import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/responsive_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomSermonBoxLayout extends StatelessWidget {
  final String month;
  final String day;
  final String sermonTitle;
  final String sermonChapter;
  final String sermonDate;
  final String sermonPreacher;
  final Function() onTap;
  final bool isShow;
  final num turns;

  const CustomSermonBoxLayout(
      {Key key,
      this.month,
      this.day,
      this.sermonTitle,
      this.sermonChapter,
      this.turns = 3,
      this.isShow = false,
      this.sermonDate,
      this.sermonPreacher,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
            height: UiStyle.blockSizeHorizontal * 14,
            width: UiStyle.blockSizeHorizontal * 100,
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                    width: 0.2,
                    color: Colors.black,
                  ),
                  top: BorderSide(
                    width: 0.2,
                    color: Colors.black,
                  )),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(
                  children: [
                    Container(
                      width: UiStyle.blockSizeHorizontal * 14,
                      color: Colors.black12,
                    ),
                    Positioned(
                      left: 2,
                      top: 2,
                      child: ResponsiveTextWidget(
                        width: month.length < 2
                            ? UiStyle.blockSizeHorizontal * 6
                            : UiStyle.blockSizeHorizontal * 9,
                        height: UiStyle.blockSizeHorizontal * 9,
                        fontWeight: FontWeight.w900,
                        fontColor: Colors.white,
                        title: month,
                      ),
                    ),
                    Positioned(
                      right: 2,
                      bottom: 2,
                      child: ResponsiveTextWidget(
                        width: day.length < 2
                            ? UiStyle.blockSizeHorizontal * 6
                            : UiStyle.blockSizeHorizontal * 9,
                        height: UiStyle.blockSizeHorizontal * 9,
                        fontWeight: FontWeight.w900,
                        fontColor: Color(0xffe08484),
                        title: day,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(top: UiStyle.blockSizeHorizontal),
                      child: SizedBox(
                        width: UiStyle.blockSizeHorizontal * 76,
                        height: UiStyle.blockSizeHorizontal * 8,
                        child: Center(
                          child: Text(
                            sermonTitle, style: TextStyle(
                              fontSize: UiStyle.blockSizeHorizontal * 5,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            bottom: UiStyle.blockSizeHorizontal - 0.4),
                        child: SizedBox(
                          height: UiStyle.blockSizeHorizontal * 4,
                          child: Text(
                            sermonChapter,
                            style: TextStyle(
                              fontSize: UiStyle.blockSizeHorizontal * 3,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        )),
                  ],
                ),
                Container(
                  width: UiStyle.safeBlockHorizontal * 10,
                  child: Center(
                    child: RotatedBox(
                      quarterTurns: turns,
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black12,
                      ),
                    ),
                  ),
                ),
              ],
            )),
        isShow
            ? SizedBox(
                height: UiStyle.blockSizeHorizontal * 30,
                child: Row(
                  children: [
                    Container(
                      width: UiStyle.blockSizeHorizontal * 65,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: Icon(
                                  FontAwesomeIcons.quoteLeft,
                                  size: 14,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(sermonTitle, style: TextStyle(
                                fontSize: sermonTitle.length > 17 ? 12 : 14,
                              ),),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: Icon(
                                  FontAwesomeIcons.bible,
                                  size: 14,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(sermonChapter),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: Icon(
                                  FontAwesomeIcons.calendarDay,
                                  size: 14,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                sermonDate,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: Icon(
                                  FontAwesomeIcons.userAlt,
                                  size: 14,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                sermonPreacher,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                          width: UiStyle.blockSizeHorizontal * 35,
                          child: Image.asset(
                            'img/home_view_img/video_player_background.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                        GestureDetector(
                          onTap: onTap,
                          child: Center(
                            child: Container(
                              height: UiStyle.blockSizeHorizontal * 35,
                              width: UiStyle.blockSizeHorizontal * 35,
                              child: Image.asset(
                                'img/home_view_img/play-botton.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            : Container(),
      ],
    );
  }
}
