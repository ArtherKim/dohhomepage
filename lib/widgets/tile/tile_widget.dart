import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:doh_homepage_app/widgets/responsive_text.dart';
import 'package:flutter/material.dart';

class TileWidget extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function onTap;
  final Color iconColor;

  const TileWidget({Key key, this.icon, this.title, this.onTap, this.iconColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: UiStyle.blockSizeHorizontal*35,
        width: UiStyle.blockSizeHorizontal*33,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.white,
              width: 0.5,
            ),
            right: BorderSide(
              color: Colors.white,
              width: 0.5,
            )
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              height: UiStyle.blockSizeHorizontal* 25,
              child: Stack(
                children: [
                  Positioned(
                    top: UiStyle.safeBlockHorizontal * 5,
                    left: UiStyle.safeBlockHorizontal * 7.5,
                    child: SizedBox(
                      height: UiStyle.blockSizeHorizontal * 15,
                      width: UiStyle.blockSizeHorizontal * 15,
                      child: FittedBox(
                        child: Icon(
                          icon,
                          color: iconColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: UiStyle.blockSizeHorizontal*10 - 0.5,
              color: UiStyle.videoPlayerBackgroundColor,
              child: Stack(
                children: [
                  Positioned(
                    top: UiStyle.blockSizeHorizontal*2,
                    left: title.length > 3 ? UiStyle.blockSizeHorizontal*7.5 : UiStyle.blockSizeHorizontal*11.5,
                    child: ResponsiveTextWidget(
                      height: UiStyle.blockSizeHorizontal*6,
                      width: title.length > 3 ? UiStyle.blockSizeHorizontal*18 : UiStyle.blockSizeHorizontal*9,
                      fontWeight: FontWeight.bold,
                      fontColor: Colors.white,
                      title: title,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}