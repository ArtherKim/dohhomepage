import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class LocatorButtonFrontWidget extends StatelessWidget {
  final bool enabled;

  const LocatorButtonFrontWidget({Key key, this.enabled}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TranslationAnimatedWidget(
      enabled: enabled,
      values: [
        Offset(100, 0), /// disabled value value
        Offset(100, 0), /// intermediate value
        Offset(0, 0)
      ],
      child: GestureDetector(
        child: Container(
          height: UiStyle.safeBlockHorizontal*13,
          width: UiStyle.safeBlockHorizontal*13,
          decoration: BoxDecoration(
              color: const Color(0xff555555),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                topLeft: Radius.circular(50),
              )),
          child: Icon(
            FontAwesomeIcons.chevronCircleLeft,
            color: Colors.white,
            size: 30,
          ),
        ),
      ),
    );
  }
}

class LocatorButtonBackWidget extends StatelessWidget {
  final bool enabled;
  final List<String> widgetList;
  final ItemScrollController itemScrollController;

  const LocatorButtonBackWidget({Key key, this.enabled, this.widgetList, this.itemScrollController}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TranslationAnimatedWidget(
      enabled: enabled,
      values: [
        Offset(0, 0), // disabled value value
        Offset(200, 0), //intermediate value
        Offset(200, 0)
      ],
      child: Container(
        padding: const EdgeInsets.all(10.0),
        width: UiStyle.safeBlockHorizontal * 35,
        decoration: BoxDecoration(
            color: const Color(0xff555555),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              topLeft: Radius.circular(20),
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for(int i = 0; i<widgetList.length; i++)
            InkWell(
              onTap: () {
                itemScrollController.scrollTo(
                    index: i,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.decelerate
                );
              },
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  widgetList[i],
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

