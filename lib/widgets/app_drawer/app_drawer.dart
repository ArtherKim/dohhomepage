import 'package:doh_homepage_app/ui/responsive/orientation_layout.dart';
import 'package:doh_homepage_app/ui/responsive/screen_type_layout.dart';
import 'package:doh_homepage_app/widgets/app_drawer/app_drawer_mobile_layout.dart';
import 'package:doh_homepage_app/widgets/app_drawer/app_drawer_tablet/app_drawer_tablet_landscape.dart';
import 'package:doh_homepage_app/widgets/app_drawer/app_drawer_tablet/app_drawer_tablet_portrait.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: AppDrawerMobileLayout(),
      tablet: OrientationLayout(
        portrait: AppDrawerTabletPortrait(),
        landscape: AppDrawerTabletLandscape(),
      ),
    );
  }
}