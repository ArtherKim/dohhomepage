import 'package:flutter/material.dart';

class CustomViewBoxLayout extends StatelessWidget {
  final Widget titleArea;
  final Widget buttonsArea;
  final Widget bodyArea;
  final GlobalKey globalKey;

  const CustomViewBoxLayout({
    Key key,
    this.titleArea,
    this.buttonsArea,
    this.bodyArea,
    this.globalKey,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: globalKey,
      child: Column(
        children: [
          titleArea,
          buttonsArea,
          bodyArea,
        ],
      ),
    );
  }
}

class CustomViewBoxTitleLayout extends StatelessWidget {
  final String title;
  final double height;
  final double fontSize;

  const CustomViewBoxTitleLayout({
    Key key,
    this.title,
    this.height = 25,
    this.fontSize = 18,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: height,
          width: 15,
          color: const Color(0xFF6fc8d2),
        ),
        Padding(
          padding: const EdgeInsets.only(left:15.0),
          child: Text(title, style: TextStyle(fontSize: fontSize, color: const Color(0xFF599ba2), fontWeight: FontWeight.w400),),
        ),
      ],
    );
  }
}

class CustomViewBoxBodyLayout extends StatelessWidget {
  final Widget child;

  const CustomViewBoxBodyLayout({
    Key key, this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:10.0,bottom: 30.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: const Color(0xfff7f5ea),
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            )
        ),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
            child: child /*Text('준비중입니다.',style: TextStyle(fontSize: 15,),),*/
        ),
      ),
    );
  }
}

class CustomViewBoxButtonLayout extends StatelessWidget {
  final Widget buttons;

  const CustomViewBoxButtonLayout({
    Key key, this.buttons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buttons;
  }
}

