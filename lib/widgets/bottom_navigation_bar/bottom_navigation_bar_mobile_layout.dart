import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class BottomNavigationBarMobileLayout extends StatelessWidget {

  final int currentIndex;
  final Function(int) onTap;

  const BottomNavigationBarMobileLayout({Key key, this.currentIndex, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 0.0,
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      unselectedIconTheme: IconThemeData(color: Colors.white),
      selectedIconTheme: IconThemeData(color: UiStyle.activeBottomNavigationOthersColor),
      onTap: onTap,
      items: [
        BottomNavigationBarItem(
          activeIcon: BottomNavigationItemMobile(
            icons: Icon(FontAwesomeIcons.home, size: 25, color:UiStyle.activeBottomNavigationHomeColor,),
            color:  UiStyle.backgroundBottomNavigationHomeColor,
          ),
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons: Icon(FontAwesomeIcons.home, size: 25,),
            color:  UiStyle.backgroundBottomNavigationHomeColor,
          ),
        ),
        BottomNavigationBarItem(
          backgroundColor: UiStyle.backgroundBottomNavigationOthersColor,
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons: Padding(
              padding: const EdgeInsets.fromLTRB(8,6,8,6),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(FontAwesomeIcons.church, size: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(FontAwesomeIcons.fish, size: 20,),
                      Icon(FontAwesomeIcons.graduationCap, size: 20,),
                    ],
                  )
                ],
              ),
            ),
            color:  UiStyle.backgroundBottomNavigationOthersColor,
          ),
        ),
        BottomNavigationBarItem(
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons: Icon(FontAwesomeIcons.bible, size: 25,),
          ),),
        BottomNavigationBarItem(
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons: Icon(FontAwesomeIcons.calendarAlt, size: 25,),
          ),),
        BottomNavigationBarItem(
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons:Icon(FontAwesomeIcons.images),),
          ),
        BottomNavigationBarItem(
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            color: const Color(0xFFFFD400),
            icons:Icon(FontAwesomeIcons.child),),
        ),
        BottomNavigationBarItem(
          title: Padding(padding: EdgeInsets.all(0)),
          icon: BottomNavigationItemMobile(
            icons: Icon(FontAwesomeIcons.list),),
        ),
      ],
    );
  }
}

class BottomNavigationItemMobile extends StatelessWidget {

  final Widget icons;
  final Color color;

  const BottomNavigationItemMobile({Key key, this.icons, this.color = const Color(0xFFc7d053)}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(0),
        height: UiStyle.safeBlockVertical*7,
        width: UiStyle.safeBlockHorizontal*100/7,
        color: color,
        child: icons
    );
  }
}
