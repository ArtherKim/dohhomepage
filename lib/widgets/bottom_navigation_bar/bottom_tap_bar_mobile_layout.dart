import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class BottomTapBarMobileLayout extends StatelessWidget {

  final Widget child;
  final Function onTap;
  final Function onDoubleTap;


  const BottomTapBarMobileLayout({Key key,this.onTap, this.child, this.onDoubleTap,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      onDoubleTap: onDoubleTap,
      child: Container(
        width: UiStyle.safeBlockHorizontal * 14,
        child: child,
      ),
    );
  }
}

class BottomNavigationItemMobile extends StatelessWidget {

  final Widget icons;
  final Color color;

  const BottomNavigationItemMobile({Key key, this.icons, this.color = const Color(0xFFc7d053)}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(0),
        height: UiStyle.safeBlockVertical*7.5,
        width: UiStyle.safeBlockHorizontal*10,
        color: color,
        child: icons
    );
  }
}
