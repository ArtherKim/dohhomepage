import 'package:flutter/material.dart';

class ResponsiveTextWidget extends StatelessWidget {
  final double width;
  final double height;
  final String title;
  final Color fontColor;
  final FontWeight fontWeight;

  const ResponsiveTextWidget({
    Key key,
    this.width,
    this.height,
    this.title,
    this.fontColor = Colors.black,
    this.fontWeight = FontWeight.normal,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: FittedBox(
        fit: BoxFit.fill,
        child: Text(
          title, style: TextStyle(
            color: fontColor,
            fontWeight: fontWeight,
          ),
        ),
      ),
    );
  }
}
