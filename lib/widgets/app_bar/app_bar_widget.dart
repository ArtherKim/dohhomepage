import 'package:doh_homepage_app/utils/ui_style.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppBarLayout extends StatelessWidget {
  final IconData icon;
  final Color backgroundColor;
  final String title;
  final Widget button1;
  final Widget button2;
  final Widget button3;

  const AppBarLayout({Key key, this.icon, this.title, this.button1, this.button2, this.backgroundColor, this.button3}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Stack(
        children: [
          AppBar(
            backgroundColor: Colors.white,
            title: Container(),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(UiStyle.safeBlockHorizontal),
              child: Container(
                height: UiStyle.blockSizeHorizontal * 12,
                width: UiStyle.width,
                color: backgroundColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: UiStyle.blockSizeHorizontal * 15,
                      child: Icon(
                        icon,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                    Container(
                      width: UiStyle.blockSizeHorizontal * 45,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          title,
                          style: TextStyle(
                              fontSize: 23,
                              color: Colors.white
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: UiStyle.blockSizeHorizontal*40,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          button1 == null ? Container() : button1,
                          button2 == null ? Container() : button2,
                          button3 == null ? Container() : button3,
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            elevation: 0.0,
            centerTitle: true,
            automaticallyImplyLeading: false,
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: SizedBox(
                height: UiStyle.blockSizeHorizontal * 10,
                child: Image.asset('img/home_view_img/logo.png',
                    fit: BoxFit.cover),
              ),
            ),
          ),
        ],
      );
  }
}

class TailButtonLayout extends StatelessWidget {
  final double width;
  final String title;
  final Color backgroundColor;
  final VoidCallback onTap;

  const TailButtonLayout({Key key, this.width, this.title, this.backgroundColor, this.onTap}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: UiStyle.blockSizeHorizontal * 12,
        color: backgroundColor,//Color(0xff9e5e5e),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontSize: 20,
                color: Colors.white
            ),
          ),
        ),
      ),
    );
  }
}
