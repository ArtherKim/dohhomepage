// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../ui/views/church_info/church_info_view.dart';
import '../ui/views/home/home_view.dart';
import '../ui/views/push_notification_test.dart';
import '../ui/views/startup/startup_view.dart';
import '../ui/views/startup/startup_view_model.dart';

class Routes {
  static const String startupView = '/';
  static const String homeView = '/home-view';
  static const String churchInfoView = '/church-info-view';
  static const String pushNotificationTest = '/push-notification-test';
  static const all = <String>{
    startupView,
    homeView,
    churchInfoView,
    pushNotificationTest,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startupView, page: StartupView),
    RouteDef(Routes.homeView, page: HomeView),
    RouteDef(Routes.churchInfoView, page: ChurchInfoView),
    RouteDef(Routes.pushNotificationTest, page: PushNotificationTest),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    StartupView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const StartupView(),
        settings: data,
      );
    },
    HomeView: (data) {
      final args = data.getArgs<HomeViewArguments>(
        orElse: () => HomeViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomeView(
          key: args.key,
          startupViewModel: args.startupViewModel,
        ),
        settings: data,
      );
    },
    ChurchInfoView: (data) {
      final args = data.getArgs<ChurchInfoViewArguments>(
        orElse: () => ChurchInfoViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ChurchInfoView(
          key: args.key,
          startupViewModel: args.startupViewModel,
        ),
        settings: data,
      );
    },
    PushNotificationTest: (data) {
      final args = data.getArgs<PushNotificationTestArguments>(
        orElse: () => PushNotificationTestArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => PushNotificationTest(
          key: args.key,
          postId: args.postId,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// HomeView arguments holder class
class HomeViewArguments {
  final Key key;
  final StartupViewModel startupViewModel;
  HomeViewArguments({this.key, this.startupViewModel});
}

/// ChurchInfoView arguments holder class
class ChurchInfoViewArguments {
  final Key key;
  final StartupViewModel startupViewModel;
  ChurchInfoViewArguments({this.key, this.startupViewModel});
}

/// PushNotificationTest arguments holder class
class PushNotificationTestArguments {
  final Key key;
  final String postId;
  PushNotificationTestArguments({this.key, this.postId});
}
