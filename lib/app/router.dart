import 'package:auto_route/auto_route_annotations.dart';
import 'package:doh_homepage_app/ui/views/church_info/church_info_view.dart';
import 'package:doh_homepage_app/ui/views/home/home_view.dart';
import 'package:doh_homepage_app/ui/views/push_notification_test.dart';
import 'package:doh_homepage_app/ui/views/startup/startup_view.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    MaterialRoute(page: StartupView, initial: true, name: 'startupView'),
    MaterialRoute(page: HomeView, name: 'homeView'),
    MaterialRoute(page: ChurchInfoView, name: 'churchInfoView'),
    MaterialRoute(page: PushNotificationTest, name: 'pushNotificationTest'),
  ],
)
class $Router {}