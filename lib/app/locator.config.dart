// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';

import '../ui/navigation_function_model.dart';
import '../api/rest_api_helper.dart';
import '../ui/views/startup/startup_view_model.dart';
import '../service/shared_preference.dart';
import '../service/third_party_service_module.dart';
import '../ui/luncher_function_model.dart';
import '../utils/ui_style.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final thirdPartyServicesModule = _$ThirdPartyServicesModule();
  gh.lazySingleton<DialogService>(() => thirdPartyServicesModule.dialogService);
  gh.lazySingleton<NavigationFunctionModel>(() => NavigationFunctionModel());
  gh.lazySingleton<NavigationService>(
      () => thirdPartyServicesModule.navigationService);
  gh.lazySingleton<RestAPIHelper>(() => RestAPIHelper());
  gh.lazySingleton<SnackbarService>(
      () => thirdPartyServicesModule.snackBarService);
  gh.lazySingleton<StartupViewModel>(() => StartupViewModel());
  gh.lazySingleton<StorageUtil>(() => StorageUtil());
  gh.lazySingleton<URLLauncherModel>(() => URLLauncherModel());
  gh.lazySingleton<UiStyle>(() => UiStyle());
  return get;
}

class _$ThirdPartyServicesModule extends ThirdPartyServicesModule {
  @override
  DialogService get dialogService => DialogService();
  @override
  NavigationService get navigationService => NavigationService();
  @override
  SnackbarService get snackBarService => SnackbarService();
}
